<?php
class ResultSet implements ArrayAccess, Countable, SeekableIterator
{
	protected $offset;
	protected $result;
	protected $numRows;
	protected $className;
	
	public function __construct(mysqli_result $result)
	{
		$this->result = $result;
		$this->numRows = $result->num_rows;
		$this->offset = 0;
	}
	
	/**
	 * Return the whole resultset as array (memory intensive)
	 * 
	 * @return array
	 */
	public function getAsArray()
	{
		return iterator_to_array($this, true);
	}
	
	public function first()
	{
		$this->rewind();
		return $this->current();
	}
	
	/**
	 * @param offset
	 */
	public function offsetExists ($offset)
	{
		return $offset < $this->numRows;
	}

	/**
	 * @param offset
	 */
	public function offsetGet ($offset)
	{
		$this->result->data_seek($offset);
		return !empty($this->className) ? $this->result->fetch_object($this->className) : $this->result->fetch_object();
	}

	/**
	 * @param offset
	 * @param value
	 */
	public function offsetSet ($offset, $value)
	{
		throw new BadMethodCallException('Cant set a key in a resultset');
	}

	/**
	 * @param offset
	 */
	public function offsetUnset ($offset)
	{
		throw new BadMethodCallException('Cant unset a key in a resultset');
	}
	
	/**
	 * @param position
	 */
	public function seek ($position)
	{
		$this->result->data_seek($position);
	}

	public function current ()
	{
		return !empty($this->className) ? $this->result->fetch_object($this->className) : $this->result->fetch_object();
	}

	public function next ()
	{
		$this->offset++;
	}

	public function key ()
	{
		return $this->offset;
	}

	public function valid ()
	{
		return $this->offset < $this->numRows;
	}

	public function rewind ()
	{
		$this->offset = 0;
		$this->result->data_seek(0);
	}
	
	public function count ()
	{
		return $this->numRows;
	}
}