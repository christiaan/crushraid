<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid <http://github.com/christiaan/crushraid>
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Wowheroes_Exception extends Exception{}

/**
 * Simple static class to load usefull data from Wow-heroes.com
 * @author Christiaan Baartse <christiaan@baartse.nl>
 * @version 2.0
 */
class Wowheroes
{
	protected static $_browser = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.2) Gecko/20070319 Firefox/2.0.0.3";
	protected static $_timeout = 15;
	
	/**
	 * @var array Classes the way the armory handles them alphabetically sorted
	 */
	public static $classes = array(
		6 => array(
			'name' => 'Death Knight',
			'color' => 'C41F3B',
		),
		11 => array(
			'name' => 'Druid',
			'color' => 'FF7D0A',
		),
		3 => array(
			'name' => 'Hunter',
			'color' => 'ABD473',
		),
		8 => array(
			'name' => 'Mage',
			'color' => '69CCF0',
		),
		2 => array(
			'name' => 'Paladin',
			'color' => 'F58CBA',
		),
		5 => array(
			'name' => 'Priest',
			'color' => 'FFFFFF',
		),
		4 => array(
			'name' => 'Rogue',
			'color' => 'FFF569',
		),
		7 => array(
			'name' => 'Shaman',
			'color' => '2459FF',
		),
		9 => array(
			'name' => 'Warlock',
			'color' => '9482C9',
		),
		1 => array(
			'name' => 'Warrior',
			'color' => 'C79C6E',
		),
	);
	
	/**
	 * @var array Races as the Armory maps them to ids
	 */
	public static $races = array(
		1 => 'Human',
		3 => 'Dwarf',
		4 => 'Night Elf',
		7 => 'Gnome',
		11 => 'Dreanei',
		2 => 'Orc',
		5 => 'Undead',
		6 => 'Tauren',
		8 => 'Troll',
		10 => 'Blood Elf',
	);
	
	public static $gender = array(
		0 => 'Male',
		1 => 'Female'
	);
	
 	protected static function tryGet($array, $key, $default = '')
 	{
 		return isset($array[$key]) ? (string) $array[$key] : $default;
 	}
	
 	protected static function requestXml($url)
 	{
 		$ch = curl_init();
 		curl_setopt ($ch, CURLOPT_URL, $url);
 		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
 		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, self::$_timeout);
 		curl_setopt ($ch, CURLOPT_USERAGENT,  self::$_browser);
 		 
 		$url_string = curl_exec($ch);
 		curl_close($ch);

 		return $url_string;
 	}
	
 	/**
 	 * Get the guildlisting from Wowheroes
 	 * 
 	 * @throws Wowheroes_Exception
 	 * @param string $realm The realm the guild is on
 	 * @param string $guild Guildname
 	 * @param string $zone Zone the realm is in
 	 * @return array An array containing all characters
 	 */
 	public static function getGuild($realm, $guild, $zone = 'eu')
 	{
 		$xml_string = self::requestXml(sprintf(
 		  'http://xml.wow-heroes.com/xml-guild.php?z=%s&r=%s&g=%s', // Zone, Realm, Guildname
 		  strtolower($zone), ucfirst($realm), ucfirst($guild)));

 		if(!$xml_string) {
 			throw new Wowheroes_Exception('Wowheroes did not respond');
 		}
 		
 		$xml = simplexml_load_string($xml_string);
 		if(!isset($xml->guild->character)) {
 			throw new Wowheroes_Exception('Xml not formatted as expected');
 		}
 		
 		$characters = array();
 		foreach ($xml->guild->character as $data) {
 			$c = array();
 			$c['name'] = self::tryGet($data, 'name');
 			$c['score']	= self::tryGet($data, 'score', 0);
 			$c['rank'] = self::tryGet($data, 'guildRank');
 			$c['specId'] = self::tryGet($data, 'specId');
 			$c['spec'] = self::tryGet($data, 'specName');
 			$c['genderId'] = self::tryGet($data, 'genderId');
 			$c['gender'] = self::tryGet(self::$gender, $c['genderId']);
 			$c['classId'] = self::tryGet($data, 'classId');
 			$class = isset(self::$classes[$c['classId']]) ?
 			  self::$classes[$c['classId']] : array();
 			$c['class'] = self::tryGet($class, 'name');
 			$c['classColor'] = self::tryGet($class, 'color');
 			$c['talents'] = self::tryGet($data, 'talents');
 			$c['date'] = self::tryGet($data, 'date');
 			$c['suggest'] = explode(';', self::tryGet($data, 'suggest'));
 			$characters[] = $c;
		}
 		
 		return $characters;
 	}
}