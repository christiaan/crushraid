<?php
class ActiveRecordIterator implements SeekableIterator, ArrayAccess, Countable
{
	/**
	 * @var string The classname used to make the objects
	 */
	protected $className;
	
	/**
	 * @var ResultSet Database Resultset
	 */
	protected $resultSet;
	
	/**
	 * Constructor
	 *
	 * @param string $classname
	 * @param ResultSet $resultSet
	 */
	public function __construct($classname, ResultSet $resultSet)
	{
		if(is_object($classname)) {
			$classname = get_class($classname);
		}
		$this->className = $classname;
		$this->resultSet = $resultSet;
	}
	
	/**
	 * Get the resultset as array
	 *
	 * @return array
	 */
	public function getAsArray()
	{
		return ActiveRecord::factory($this->className)->loadResult($this->resultSet->getAsArray());
	}
	
	/**
	 * Get a array with key value pairs
	 * 
	 * @param string $key Column used as key
	 * @param string $value Column used as value
	 * @return array
	 */
	public function getSelect($key, $value)
	{
		$select_array = array();
		foreach($this as $row)
		{
			if(!empty($key))
			{
				$select_array[$row[$key]] = $row[$value];
			}
			else
			{
				$select_array[] = $row[$value];
			}
		}
		return $select_array;
	}
	
	/**
	 * Countable: count
	 */
	public function count()
	{
		return $this->resultSet->count();
	}
	
	/**
	 * Iterator: current
	 * 
	 * @return ActiveRecord
	 */
	public function current()
	{
		if (FALSE !== ($row = $this->resultSet->current()))
		{
			$row = ActiveRecord::factory($this->className)->loadResult($row);
		}
		
		return $row;
	}
	
	/**
	 * SeekableIterator: seek
	 */
	public function seek($position)
	{
		return $this->resultSet->seek($position);
	}
	
	/**
	 * Iterator: key
	 */
	public function key()
	{
		return $this->resultSet->key();
	}
	
	/**
	 * Iterator: next
	 * 
	 * @return ActiveRecord
	 */
	public function next()
	{
		return $this->resultSet->next();
	}
	
	/**
	 * Iterator: rewind
	 */
	public function rewind()
	{
		return $this->resultSet->rewind();
	}
	
	/**
	 * Iterator: valid
	 */
	public function valid()
	{
		return $this->resultSet->valid();
	}
	
	/**
	 * ArrayAccess: offsetExists
	 */
	public function offsetExists($offset)
	{
		return $this->resultSet->offsetExists($offset);
	}
	
	/**
	 * ArrayAccess: offsetGet
	 *
	 * @param int $offset
	 * @return ActiveRecord
	 */
	public function offsetGet($offset)
	{
		if( $this->resultSet->offsetExists($offset))
		{
			return ActiveRecord::factory($this->className)->loadResult($this->resultSet->offsetGet($offset));
		}
	}
	
	/**
	 * ArrayAccess: offsetSet
	 */
	public function offsetSet($offset, $value)
	{
		return false;
	}
	
	/**
	 * ArrayAccess: offsetUnset
	 */
	public function offsetUnset($offset)
	{
		return false;
	}
	
	public function limit($limit, $page = 0)
	{
		$last_index = $limit * ($page+1);
		$arr = array();
		
		for($i = $limit * $page; $i < $last_index; $i++)
		{
			if(!$this->offsetExists($i)) break;
			$arr[] = $this[$i];
		}
		
		return (array) $arr;
	}
}
