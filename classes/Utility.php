<?php
class Utility
{
	/**
	 * Get a given array value by key or return $default if not exist
	 * 
	 * @param array $array
	 * @param string $key
	 * @param mixedvar $default
	 * @return mixedvar
	 */
	static public function arrayValue($array, $key, $default = null)
	{
		return isset($array[$key]) ? $array[$key] : $default;
	}
	
	static public function redirect($url)
	{
		header("Location: ".$url);
		exit;
	}

	/**
	 * Convert a pile of seconds to years days hours minutes and seconds
	 * 
	 * @param int $sec Number of seconds
	 * @return array Array containing years days hours minutes and seconds
	 */
	static public function sec2Time($time)
	{
		if(is_numeric($time)) {
			$value = array(
	      		'years' => 0,
	      		'days' => 0,
	      		'hours' => 0,
	      		'minutes' => 0,
	      		'seconds' => 0,
			);
			if($time >= 31556926) {
				$value['years'] = floor($time/31556926);
				$time = ($time%31556926);
			}
			if($time >= 86400) {
				$value['days'] = floor($time/86400);
				$time = ($time%86400);
			}
			if($time >= 3600) {
				$value['hours'] = floor($time/3600);
				$time = ($time%3600);
			}
			if($time >= 60) {
				$value['minutes'] = floor($time/60);
				$time = ($time%60);
			}
			$value['seconds'] = floor($time);
			return $value;
		}
		
		return FALSE;
	}
}
