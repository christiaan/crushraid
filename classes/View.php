<?php
class View
{
	protected $template = '';
	
	protected $values = array();
	protected static $global_values = array();
	
	public function __construct($template, array $values = array())
	{
		$this->template = $template;
		
		$this->values = $values;
	}
	
	public function set($key, $value = null)
	{
		if(is_array($key) || $key instanceof Iterator)
		{
			foreach($key as $k => $v)
			{
				$this->__set($k, $v);
			}			
		}
		else
		{
			$this->__set($key, $value);
		}
		
		return $this;
	}
	
	public function setGlobal($key, $value = null)
	{
		if(is_array($key) || $key instanceof Iterator)
		{
			foreach($key as $k => $v)
			{
				self::$global_values[$k] = $v;
			}
		}
		else
		{
			self::$global_values[$key] = $value;
		}
		
		return $this;
	}
	
	public function __set($name, $value)
	{
		if(!isset($this->$name))
		{
			$this->values[$name] = $value;
		}
	}
	
	public function __get($name)
	{
		if(isset($this->values[$name]))
			return $this->values[$name];
		
		if(isset(self::$global_values[$name]))
			return self::$global_values[$name];
			
		if(isset($this->$name))
			return $this->$name;
	}
	
	public function render($echo = false)
	{
		ob_start();
		extract(self::$global_values);
		extract($this->values);
		include(CrushRaid::c('views_dir').DIRECTORY_SEPARATOR.$this->template.'.php');
		if(!$echo) return (string) ob_get_clean();
		else ob_flush();
	}
	
	public function __toString()
	{
		return $this->render(false);
	}
}