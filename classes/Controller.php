<?php
class Controller
{
	public function defaultAction()
	{
		throw new Exception('Specified action not found ' . CrushRaid::$path_info);
	}
}