<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Armory
{
	protected static $urls = array(
		'eu' => array(
			'guildroster'	=> 'http://eu.wowarmory.com/guild-info.xml?r=%s&n=%s&p=%d', // Realm Guild Page
			'character'		=> 'http://eu.wowarmory.com/character-sheet.xml?r=%s&n=%s', // Realm Name
			'calendar'		=> 'https://eu.battle.net/login/login.xml?ref=http%%3A%%2F%%2Feu.wowarmory.com%%2Fvault%%2Fcalendar%%2Fmonth-user.json%%3Fmonth%%3D%d%%26callback%%3Dcalendar.loadEvents%%26year%%3D%d%%26r%%3D%s%%26loc%%3Den_us%%26n%%3D%s&app=armory', // Month Year Realm Charname
			'calendarevent'	=> 'https://eu.battle.net/login/login.xml?ref=http%%3A%%2F%%2Feu.wowarmory.com%%2Fvault%%2Fcalendar%%2Fdetail.json%%3Fcallback%%3Dcalendar.loadEventDetail%%26r%%3D%s%%26n%%3D%s%%26e%%3D%d&app=armory', // Realm Charname Eventid
		),
		'us' => array(
			'guildroster'	=> 'http://www.wowarmory.com/guild-info.xml?r=%s&n=%s&p=%d', // Realm Guild Page
			'character'		=> 'http://www.wowarmory.com/character-sheet.xml?r=%s&n=%s', // Realm Name			
		),
	);
	
	protected static $auth = null;
	
	const BROWSER	= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.2) Gecko/20070319 Firefox/2.0.0.3";
	protected static $timeout = 15;
	
	/**
	 * Classes the way the armory handles them alphabetically sorted
	 * @var array
	 */
	public static $classes = array(
		6 => array(
			'name' => 'Death Knight',
			'color' => 'C41F3B',
		),
		11 => array(
			'name' => 'Druid',
			'color' => 'FF7D0A',
		),
		3 => array(
			'name' => 'Hunter',
			'color' => 'ABD473',
		),
		8 => array(
			'name' => 'Mage',
			'color' => '69CCF0',
		),
		2 => array(
			'name' => 'Paladin',
			'color' => 'F58CBA',
		),
		5 => array(
			'name' => 'Priest',
			'color' => 'FFFFFF',
		),
		4 => array(
			'name' => 'Rogue',
			'color' => 'FFF569',
		),
		7 => array(
			'name' => 'Shaman',
			'color' => '2459FF',
		),
		9 => array(
			'name' => 'Warlock',
			'color' => '9482C9',
		),
		1 => array(
			'name' => 'Warrior',
			'color' => 'C79C6E',
		),
	);
	
	public static $races = array(
		1 => 'Human',
		3 => 'Dwarf',
		4 => 'Night Elf',
		7 => 'Gnome',
		11 => 'Dreanei',
		2 => 'Orc',
		5 => 'Undead',
		6 => 'Tauren',
		8 => 'Troll',
		10 => 'Blood Elf',
	);
	
 	protected static function requestXml($url)
 	{
 		$ch = curl_init();
 		curl_setopt ($ch, CURLOPT_URL, $url);
 		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
 		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, self::$timeout);
 		curl_setopt ($ch, CURLOPT_USERAGENT,  self::BROWSER);
 		 
 		$url_string = curl_exec($ch);
 		curl_close($ch);

 		return (FALSE !== $url_string) ? simplexml_load_string($url_string) : false;
 	}
 	
 	public static function getCharacterUrl($realm, $name, $zone = 'eu')
 	{
 		if(isset(self::$urls[$zone]['character']))
 		{
	 		return sprintf(self::$urls[$zone]['character'], ucfirst($realm), ucfirst($name));
 		}
 		return false; 		
 	}
	
 	public static function getCharacter($realm, $name, $zone = 'eu')
 	{
 		if($url = self::getCharacterUrl($realm, $name, $zone))
 		{
	 		return self::requestXml($url);
 		}
 		return false;
 	}
 	
	public static function getGuildRosterUrl($realm, $guild, $page = 1, $zone = 'eu')
 	{
 		if(isset(self::$urls[$zone]['guildroster']))
 		{
	 		return sprintf(self::$urls[$zone]['guildroster'], ucfirst($realm), ucfirst($guild), $page);
 		}
 		return false;
 	}
 	
 	public static function getGuildRoster($realm, $guild, $page = 1, $zone = 'eu')
 	{
 		if($url = self::getGuildRosterUrl($realm, $guild, $page, $zone))
 		{
	 		return self::requestXml($url);
 		}
 		return false;
 	}
 	
 	public static function setAuth($accountname, $password)
 	{
 		self::$auth = array('accountName' => $accountname, 'password' => $password);
 	}
 	
 	public static function getCalendar($realm, $charname, $month = null, $year = null, $zone = 'eu')
 	{
 		if(is_null($month)) $month = date('n');
 		if(is_null($year)) $year = date('Y');
 		$url = sprintf(self::$urls[$zone]['calendar'], $month, $year, ucfirst($realm), ucfirst($charname));
  		$ch = curl_init();
 		curl_setopt ($ch, CURLOPT_URL, $url);
 		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
 		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, false);
 		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
 		curl_setopt ($ch, CURLOPT_MAXREDIRS, 5);
 		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
 		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, self::$timeout);
 		curl_setopt ($ch, CURLOPT_USERAGENT,  self::BROWSER);
 		curl_setopt	($ch, CURLOPT_POST, true);
	 	curl_setopt	($ch, CURLOPT_POSTFIELDS, http_build_query(self::$auth));
 		$url_string = curl_exec($ch);
 		curl_close($ch);
		
 		$url_string = substr($url_string, 20, -2);
 		
 		return json_decode($url_string);
 	}
 	
 	public static function getCalenderEvent($realm, $charname, $event_id, $zone = 'eu')
 	{
 		 $url = sprintf(self::$urls[$zone]['calendarevent'], ucfirst($realm), ucfirst($charname), $event_id);
  		$ch = curl_init();
 		curl_setopt ($ch, CURLOPT_URL, $url);
 		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
 		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, false);
 		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
 		curl_setopt ($ch, CURLOPT_MAXREDIRS, 5);
 		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
 		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, self::$timeout);
 		curl_setopt ($ch, CURLOPT_USERAGENT,  self::BROWSER);
 		curl_setopt	($ch, CURLOPT_POST, true);
	 	curl_setopt	($ch, CURLOPT_POSTFIELDS, http_build_query(self::$auth));
 		$url_string = curl_exec($ch);
 		curl_close($ch);

 		$url_string = substr($url_string, 25, -2);
 		
 		return json_decode($url_string);
 	}
}