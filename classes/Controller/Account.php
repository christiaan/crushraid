<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Account extends Controller
{
	protected $page;
	
	public function __construct()
	{
		$this->page = new View('page');
		$this->page->setGlobal('title', 'CrushRaid - Account');
	}
	
	public function indexAction(){}
	
	public function loginAction()
	{
		if(CrushRaid::userLoggedIn()) {
			Utility::redirect(CrushRaid::c('baseurl'));
		}
		else {
			$message = '';
			if(isset($_POST['username'])) {
				$message = 'Wrong credentials.';
			}
			$this->page->body = new View('login', compact('message'));
			$this->page->render(true);
		}
	}
	
	public function logoutAction()
	{
		if(isset($_GET['confirm'])) {
			if($_GET['confirm'] == 'yes') {
				CrushRaid::userLogout();
			}
			Utility::redirect(CrushRaid::c('baseurl'));
		}
		else {
			$this->page->body = new View('confirm');
			$this->page->body->message = 'Are you sure you want to log out?';
			$this->page->render(true);
		}
	}
}