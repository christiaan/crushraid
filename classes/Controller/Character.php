<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Character extends Controller
{
	protected $page;
	
	public function __construct()
	{
		$this->page = new View('page');
		$this->page->setGlobal('title', 'CrushRaid - Character');
	}

	public function indexAction()
	{
		$characters = DB::query(sprintf('
			SELECT
				c.`id`,
				c.`name`,
				cc.`color`,
				cc.`name` as `class_name`,
				cc.`icon` as `class_icon`,
				c.spec,
				cr.name AS role,
				cr.icon AS role_icon,
				cor.name AS offrole,
				cor.icon AS offrole_icon,
				c.in_guild,
				c.rank_id,
				r.name AS rank,
				c.gear_score,
				c.`dkp`,
				SUM(UNIX_TIMESTAMP(a.endtime)-UNIX_TIMESTAMP(a.starttime)) AS raided
			FROM
				`%1$scharacter` AS c
			LEFT JOIN
				`%1$scharacter_class` AS cc
			ON
				c.class_id = cc.id
			LEFT JOIN
				`%1$scharacter_role` AS cr
			ON
				c.role_id = cr.id
			LEFT JOIN
				`%1$scharacter_role` AS cor
			ON
				c.offrole_id = cor.id
			LEFT JOIN
				`%1$srank` AS r
			ON
				c.rank_id = r.id
			LEFT JOIN
				`%1$sraid_attendee` AS a
			ON
				a.character_id = c.id
			AND
				a.endtime != "0000-00-00 00:00:00"
			AND
				a.standby = 0
			AND
				(a.endtime BETWEEN "%2$s" AND "%3$s")
			AND
				a.raid_id IN (SELECT id FROM `%1$sraid` WHERE progress = 1)
			WHERE
				c.active = %4$d
			GROUP BY
				c.id
			ORDER BY
				cc.`name`,
				c.`dkp` DESC
			',
			CrushRaid::c('table_prefix'),
			date('Y-m-d 00:00:00', strtotime(!empty($_GET['start']) ? $_GET['start'] : '-2 weeks')),
			date('Y-m-d 23:59:59', !empty($_GET['end']) ? strtotime($_GET['end']) : time()),
			isset($_GET['active']) ? $_GET['active'] : 1
		));
		
		$this->page->body = new View('character/index', compact('characters'));
		$this->page->render(true);
	}
	
	public function viewAction($character_id)
	{
		$character = ActiveRecord::factory('Model_Character', (int) $character_id);
		if(!$character->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/character');
		
		$this->page->body = new View('character/view', compact('character'));
		
		$this->page->render(true);
	}
	
	public function editAction($character_id = null)
	{
		CrushRaid::assertLoggedIn();
		
		$character = !empty($character_id) ? ActiveRecord::factory('Model_Character', $character_id) : ActiveRecord::factory('Model_Character');
		
		$name = trim(Utility::arrayValue($_POST, 'name'));
		if(!empty($name))
		{
			$character['name'] = ucfirst($name);
			$character['parent_id'] = !empty($_POST['parent_id']) ? $_POST['parent_id'] : null;
			$character['active'] = (int) !empty($_POST['active']);
			$character['role_id'] = $_POST['role_id'];
			$character['offrole_id'] = $_POST['offrole_id'];
			$character['class_id'] = $_POST['class_id'];
			$character['race_id'] = $_POST['race_id'];
			$character['gender'] = $_POST['gender'];
			$character['level'] = $_POST['level'];
			$character['in_guild'] = (int) !empty($_POST['in_guild']);
			
			if(empty($_POST['class_id']) || empty($_POST['race_id'])) {
				$character->updateFromArmory();
			}
			
			$character->save();
			Utility::redirect(CrushRaid::c('baseurl').'/character/view/'.$character['id']);
		}
		
		$this->page->body = new View('character/edit', compact('character'));
		$this->page->render(true);
	}
	
	public function deleteAction($character_id)
	{
		CrushRaid::assertLoggedIn();
		
		$character = ActiveRecord::factory('Model_Character', (int) $character_id);
		if(!$character->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/character');
		
		if(isset($_GET['confirm']))
		{
			if($_GET['confirm'] == 'yes')
			{
				$character->delete();
			}
			Utility::redirect(CrushRaid::c('baseurl').'/character');
		}
		
		$this->page->body = new View('confirm');
		$this->page->body->message = 'Are you sure you want to delete '.
		  'character: '.$character['name'].'?';
		
		$this->page->render(true);
	}
}