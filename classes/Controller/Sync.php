<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Sync extends Controller
{
	protected $page;
	
	public function __construct()
	{
		$this->page = new View('page');
		$this->page->setGlobal('title', 'CrushRaid - Sync');
		CrushRaid::assertLoggedIn();
	}
	
	public function armoryAction()
	{
		if(isset($_GET['confirm'])) {
			if($_GET['confirm'] != 'yes') {
				Utility::redirect(CrushRaid::c('baseurl'));
			}
			$armory		= Armory::getGuildRoster(CrushRaid::c('realm'),
			  CrushRaid::c('guild'), 1, CrushRaid::c('zone'));
			$classes	= ActiveRecord::factory('Model_CharacterClass')->
			  get()->getSelect('id', 'name');
			$races		= ActiveRecord::factory('Model_CharacterRace')->
			  get()->getSelect('id', 'name');
			
			$updated	= 0;
			$added		= 0;
			
			$character_ids = array();
			if(isset($armory->guildInfo->guild->members->character))
			{
				foreach($armory->guildInfo->guild->members->character as $armory_char)
				{
					if(CrushRaid::c('level') == $armory_char['level'])
					{
						$character = ActiveRecord::factory('Model_Character')->
							where('name', $armory_char['name'])->
							where('class_id', (int) array_search(Armory::$classes[(string) $armory_char['classId']]['name'], $classes))->
						get(true);
						
						if($character->isLoaded()) {
							$updated++;
						}
						else {
							if(CrushRaid::c('add_from_armory')) {
								$character['active'] = 1;
								$added++;
							}
							else {
								continue;
							}
						}
						
						$character['class_id'] = (int) array_search(Armory::$classes[(string) $armory_char['classId']]['name'], $classes);
						$character['race_id'] = (int) array_search(Armory::$races[(string) $armory_char['raceId']], $races);
						$character['rank_id'] = $armory_char['rank'];
						$character['in_guild'] = 1;
						$character['gender'] = $armory_char['genderId'];
						$character['name'] = $armory_char['name'];
						
						if($character['level'] < (int) $armory_char['level'])
						$character['level'] = (int) $armory_char['level'];
						$character->save();
						$character_ids[] = (int) $character['id'];
					}
				}
			}
			
			if(!empty($character_ids))
			{
				$res = DB::query(sprintf('
					UPDATE `%scharacter`
					SET in_guild = 0
					WHERE id NOT IN (%s)',
					CrushRaid::c('table_prefix'),
					implode(',', $character_ids)
				));
			}
			
			$this->page->body = sprintf('
				<div>Succesfully imported armory data</div>
				<div>Members updated: %d</div>
				<div>Members added: %d</div>
				',
				$updated,
				$added
			);
		}
		else {
			$this->page->body = new View('confirm');
			$this->page->body->message = 'Are you sure you want to sync '.
			  'the character list with the Armory? <br />'.
			  (CrushRaid::c('add_from_armory') ?
			  'New characters will be added and existing ones updated' :
			  'Only existing ones will be updated' );
		}
		$this->page->render(true);
	}
	
	public function wowheroesAction()
	{
		if(isset($_GET['confirm'])) {
			if($_GET['confirm'] != 'yes') {
				Utility::redirect(CrushRaid::c('baseurl'));
			}
			$classes	= ActiveRecord::factory('Model_CharacterClass')->get()->getSelect('id', 'name');
			$updated	= 0;
			
			foreach(Wowheroes::getGuild(CrushRaid::c('realm'),
			  CrushRaid::c('guild'), CrushRaid::c('zone')) as $char) {
				$character = ActiveRecord::factory('Model_Character')->
					where('name', $char['name'])->
					where('class_id', (int) array_search($char['class'], $classes))->
				get(true);
				
				if($character->isLoaded())
				{
					$updated++;
					$character['rank_id'] = $char['rank'];
					$character['spec'] = $char['spec'];
					$character['gear_score'] = $char['score'];
					$character->save();
				}
			}
			
			$this->page->body = sprintf('
				<div>Succesfully imported Wowheroes data</div>
				<div>Members updated: %d</div>
				',
				$updated
			);
			
		}
		else {
			$this->page->body = new View('confirm');
			$this->page->body->message = 'Are you sure you want to update '.
			  'the character list from Wow-heroes.com?';
		}
		$this->page->render(true);
	}
}