<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Rest extends Controller
{
	public function indexAction()
	{
	
	}
	
	public function latestLootAction($limit = 5)
	{
		$callback = isset($_GET['callback']) ? $_GET['callback'] : 'callback';
		
		$json = array();
		
		foreach(ActiveRecord::factory('Model_RaidLoot')->limit(min(30, $limit))->get() as $loot)
		{
			$json[] = array(
				'datetime'		=> $loot['datetime'],
				'dkp'			=> (0-$loot->dkp['value']),
				'character'		=> $loot->character['name'],
				'character_color' => $loot->character->class['color'],
				'character_url'	=> 'http://'.$_SERVER['HTTP_HOST'].CrushRaid::c('baseurl').'/character/view/'.$loot['character_id'],
				'item_name'		=> $loot->item['name'], 
				'item_id'		=> $loot->item['wowid'],
				'raid'			=> $loot->raid['title'],
				'raid_url'		=> 'http://'.$_SERVER['HTTP_HOST'].CrushRaid::c('baseurl').'/raid/view/'.$loot['raid_id'],
			);
		}
		
		echo $callback.'('.json_encode($json).');'."\n";
	}
}
