<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Raid extends Controller
{
	protected $page;
	
	public function __construct()
	{
		$this->page = new View('page');
		$this->page->setGlobal('title', 'CrushRaid');
	}
	
	public function indexAction()
	{
		$raids = ActiveRecord::factory('Model_Raid')->get();
		
		$this->page->body = new View('raid/index', array('raids' => $raids));
		
		$this->page->render(true);
	}
	
	public function viewAction($raid_id)
	{
		$raid = ActiveRecord::factory('Model_Raid', $raid_id);
		if(!$raid->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/raid');
		
		$this->page->body = new View('raid/view', compact('raid'));
		
		$this->page->render(true);
	}
	
	public function editAction($raid_id)
	{
		CrushRaid::assertLoggedIn();
		
		$raid = ActiveRecord::factory('Model_Raid', $raid_id);
		if(!$raid->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/raid');
		
		if(isset($_POST['raid']['starttime']))
		{
			$this->editRaid($raid, $_POST);
		}
		
		$current_characters = array();
		foreach($raid->attendees as $attendee)
		{
			$current_characters[$attendee->character['id']] = $attendee->character['name'];
		}
		
		$this->page->body = new View('raid/edit', compact(
			'raid',
			'current_characters'
		));
		$this->page->render(true);
	}
	
	public function newAction()
	{
		CrushRaid::assertLoggedIn();
		if(isset($_POST['title']))
		{
			if(!empty($_POST['headcount']))
			{
				$xml = $_POST['headcount'];
				$headcount = simplexml_load_string($xml);
				if(false === $headcount) {
					die('Error in XML:<br /><pre>'.$xml);					
				}
			}
			$new_raid = ActiveRecord::factory('Model_Raid');
			$title = trim(Utility::arrayValue($_POST, 'title'));
			$new_raid['title'] = !empty($title) ? $title : 'No Title';
			$new_raid['starttime']	= date('Y-m-d H:i:s');
			$new_raid->save();
			
			if(isset($headcount)) {
				$new_raid->importHeadcountXml($headcount);
			}
			
			Utility::redirect(CrushRaid::c('baseurl').'/raid/edit/'.$new_raid['id']);
		}
		
		$this->page->body = new View('raid/new');
		
		$this->page->render(true);
	}
	
	public function deleteAction($raid_id)
	{
		CrushRaid::assertLoggedIn();
		
		$raid = ActiveRecord::factory('Model_Raid', $raid_id);
		if(!$raid->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/raid');
		
		if(!empty($_GET['confirm']))
		{
			if($_GET['confirm'] == 'yes')
			{
				$raid->delete();
			}
			Utility::redirect(CrushRaid::c('baseurl').'/raid');
		}
		
		$this->page->body = new View('raid/delete', compact('raid'));
		
		$this->page->render(true);
	}
	
	
	
	protected function editRaid(Model_Raid $raid, $post)
	{
		// Delete all loot and attendees that miss
		$attendee_ids = array();
		if(!empty($post['attendees']['cur']))
		{
			foreach($post['attendees']['cur'] as $id => $data)
			{
				if(!empty($data['active']))
				{
					$attendee = ActiveRecord::factory('Model_RaidAttendee', $id);
					if($attendee->isLoaded())
					{
						$attendee_ids[] = $id;
						$attendee['starttime'] = $data['starttime'];
						$attendee['endtime'] = $data['endtime'];
						$attendee['standby'] = (int) (bool) $data['standby'];
						$attendee->save();
					}
				}
			}
		}
		
		$loot_ids = array();
		if(!empty($post['loot']['cur']))
		{
			foreach($post['loot']['cur'] as $id => $data)
			{
				if(!empty($data['active']))
				{
					$loot = ActiveRecord::factory('Model_RaidLoot', $id);
					if($loot->isLoaded())
					{
						$loot_ids[] = $id;
						$loot['character_id'] = (int) $data['character_id'];
						$item = ActiveRecord::factory('Model_Item')->getItemByName($data['itemname']);
						$loot['item_id'] = $item['id'];
						$loot['datetime'] = $data['datetime'];
						$loot->dkp['value'] = 0 - $data['dkp_cost'];
						$loot->dkp['character_id'] = $loot['character_id'];
						$loot->dkp['note'] = $item['name'];
						$loot->dkp->save();
						$loot->save();
					}
				}
			}
		}
		
		$event_ids = array();
		if(!empty($post['events']['cur']))
		{
			foreach($post['events']['cur'] as $id => $data)
			{
				if(!empty($data['active']))
				{
					$event = ActiveRecord::factory('Model_RaidEvent', $id);
					if($event->isLoaded())
					{
						$event_ids[] = $id;
						$event['name']	= $data['name'];
						$event['dkp']	= $data['dkp'];
						$event['datetime'] = $data['datetime'];
						$event->save();
					}
				}
			}
		}		
		
		foreach($raid->attendees as $attendee)
		{
			if(!in_array($attendee['id'], $attendee_ids)) $attendee->delete();
		}
		
		foreach($raid->loot as $loot)
		{
			if(!in_array($loot['id'], $loot_ids)) $loot->delete();
		}
		
		foreach($raid->events as $event)
		{
			if(!in_array($event['id'], $event_ids)) $event->delete();
		}
		
		$raid->resetRelationCache();
		
		// Add new Attendees and loot
		if(!empty($post['attendees']['new']))
		{
			foreach($post['attendees']['new'] as $k => $new)
			{
				if(!is_numeric($k) || empty($new['character_id']) || empty($new['active'])) continue;
				
				$character = ActiveRecord::factory('Model_Character', (int) $new['character_id']);
				
				$raid->addAttendee(
					$character,
					Utility::arrayValue($new, 'starttime'),
					Utility::arrayValue($new, 'endtime'),
					(bool) Utility::arrayValue($new, 'standby')
				);
			}
		}
		
		if(!empty($post['loot']['new']))
		{
			foreach ($post['loot']['new'] as $k => $new)
			{
				$itemname = trim(Utility::arrayValue($new, 'itemname'));
				$character_id = (int) $new['character_id'];
				
				if(!is_numeric($k) || empty($character_id) || empty($itemname) || empty($new['active'])) continue;
				
				$raid->addLoot(
					ActiveRecord::factory('Model_Character', $character_id),
					$itemname,
					(int) Utility::arrayValue($new, 'dkp_cost'),
					Utility::arrayValue($new, 'datetime')
				);
			}
		}
		
		if(!empty($post['events']['new']))
		{
			foreach($post['events']['new'] as $k => $new)
			{
				$name = trim(Utility::arrayValue($new, 'name'));
				$dkp = (int) $new['dkp'];
				
				if(!is_numeric($k) || empty($dkp) || empty($new['active'])) continue;
				
				$raid->addEvent($name, Utility::arrayValue($new, 'datetime'), $dkp);
			}
		}
		
		// Update the raid itself
		$title = trim($post['raid']['title']);
		$raid['title'] = !empty($title) ? $title : 'No title';
		$raid['notes'] = $post['raid']['notes'];
		$raid['starttime'] = $post['raid']['starttime'];
		$raid['endtime'] = $post['raid']['endtime'];
		$raid['progress'] = (int) !empty($post['raid']['progress']);
		$raid['dkp_per_hour'] = (int) $post['raid']['dkp_per_hour'];
		$raid['standby_per_hour'] = (int) $post['raid']['standby_per_hour'];
		$raid['instance_id'] = (int) $post['raid']['instance_id'];
		
		$raid->save();
		
		if(!empty($post['finalize_raid']))
		{
			$raid->finalize(
				$raid['dkp_per_hour'],
				$raid['standby_per_hour'],
				true // true to enable refinalizing
			);
			
			Utility::redirect(CrushRaid::c('baseurl').'/raid/view/'.$raid['id']);
		}
	}
}