<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Controller_Dkp extends Controller
{
	protected $page;
	
	public function __construct()
	{
		$this->page = new View('page');
		$this->page->setGlobal('title', 'CrushRaid - Dkp');
	}

	public function viewAction($character_id, $page = 0, $limit = 100)
	{
		CrushRaid::assertLoggedIn();
		$character = ActiveRecord::factory('Model_Character', $character_id);
		
		if(!$character->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/character');
		
		$dkps = ActiveRecord::factory('Model_Dkp')->where('character_id', $character['id'])->limit($limit, $page*$limit)->get();
		
		$this->page->body = new View('dkp/view', compact('character', 'dkps'));
		$this->page->render(true);
	}
	
	public function editAction($character_id, $dkp_id = null)
	{
		CrushRaid::assertLoggedIn();
		
		$character = ActiveRecord::factory('Model_Character', $character_id);
		
		if(!$character->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/character');
		
		$dkp = !empty($dkp_id) ? ActiveRecord::factory('Model_Dkp', $dkp_id) : ActiveRecord::factory('Model_Dkp');
		
		if(isset($_POST['value']))
		{
			$dkp['character_id'] = $character['id'];
			$dkp['value'] = $_POST['value'];
			$dkp['note'] = $_POST['note'];
			$dkp->save();
			Model_Dkp::recalculateDkp($character['id']);
			Utility::redirect(CrushRaid::c('baseurl').'/dkp/view/'.$character['id']);
		}
		
		
		$this->page->body = new View('dkp/edit', compact('character', 'dkp'));
		$this->page->render(true);
	}
	
	public function deleteAction($character_id, $dkp_id)
	{
		CrushRaid::assertLoggedIn();
		
		$dkp = ActiveRecord::factory('Model_Dkp', (int) $dkp_id);
		if(!$dkp->isLoaded()) Utility::redirect(CrushRaid::c('baseurl').'/dkp/view/'.$character_id);
		
		if(!empty($_GET['confirm']))
		{
			if($_GET['confirm'] == 'yes')
			{
				$character_id = $dkp['character_id'];
				$dkp->delete();
				Model_Dkp::recalculateDkp($character_id);
			}
			Utility::redirect(CrushRaid::c('baseurl').'/dkp/view/'.$character_id);
		}
		
		$this->page->body = new View('confirm');
		$this->page->body->message = 'Are you sure you want to delete '.
		  'dkp entry: '.$dkp['note'].'?';
		$this->page->render(true);
	}
	
	public function recalculateAction()
	{
		CrushRaid::assertLoggedIn();
		if(!empty($_GET['confirm']))
		{
			if($_GET['confirm'] == 'yes')
			{
				Model_Dkp::recalculateDkp();
		
				$this->page->body = 'Succesfully recalculated everyone\'s dkp';
			}
			else {
				Utility::redirect(CrushRaid::c('baseurl'));
			}
		}
		else {
			$this->page->body = new View('confirm');
			$this->page->body->message = 'Are you sure you want to '.
			  'recalculate all dkp (this may take a while)?';
		}
		$this->page->render(true);
	}
	
	public function getdkplistAction()
	{
		define('TAB', "\t");
		define('CRLF', "\r\n");

		$output = 'DKP_tables = {} '.CRLF;
		$output.= ' DKP_tables[1] = {'.CRLF;
		
		$format = TAB.'{ nick="%s", class=DKPT_%s, current=%s, spent=%s},'.CRLF;
		$members = 0;
		foreach(ActiveRecord::factory('Model_Character')->get() as $member)
		{
			$output .= sprintf($format,
				$member['name'],
				str_replace(' ', '', strtolower($member->class['name'])),
				$member->getCurrentDkp(),
				0
			);
			$members++;
		}
		
		$output .= '};'.CRLF;
		$output .= 'DKP_tables[1].title = "Crushraid"'.CRLF;
		$output .= 'DKP_tables[1].members = '.$members.';'.CRLF.CRLF;
		$output .= 'DKP_tables_count = 1;'.CRLF;
		$output .= 'DKP_currentDate = '.time().';'.CRLF;
		$output .= 'DKP_CDateOut = "'.strtolower(date("d/M H:i:s")).'";'.CRLF;
		
		header('Content-Disposition: attachment; filename="DKP.lua"');
		echo $output;
	}
	
}
