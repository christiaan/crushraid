<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class CrushRaid
{
	public static $path_info;
	public static $path_array;
	
	public static $controller;
	public static $action;
	
	protected static $_config;
	
	public static function init($config)
	{
		self::$_config = (array) $config;
		$path_info = Utility::arrayValue($_SERVER, 'PATH_INFO');
		
		if(get_magic_quotes_gpc()) {
			function stripslashes_deep($value) {
			    return is_array($value) ?
                  array_map('stripslashes_deep', $value) :
                  stripslashes($value);
			}
			
			$_GET = stripslashes_deep($_GET);
			$_POST = stripslashes_deep($_POST);
		}
		
		self::sessionStart();
		
		self::$path_info = trim(trim($path_info), '/');
		self::$path_array = explode('/', self::$path_info);
		if(empty(self::$path_array[0])) self::$path_array = array();
		
		$controller_class = 'Controller_'.ucfirst(!empty(self::$path_array[0]) ? self::$path_array[0] : CrushRaid::c('default_controller'));
		self::$action = isset(self::$path_array[1]) ? self::$path_array[1].'Action' : 'indexAction';
		
		if( ! class_exists($controller_class))
		{
			throw new Exception('Controller couldn\'t be found');
		}
		
		self::$controller = new $controller_class();

		if(method_exists(self::$controller, self::$action))
		{
			$ref = new ReflectionMethod(self::$controller, self::$action);
			if($ref->isPublic()) 
			{
				$ref->invokeArgs(
					self::$controller,
					count(self::$path_array) > 2
						? array_slice(self::$path_array, 2)
						: array()
				);
			}
		}
		else
		{
			if(method_exists(self::$controller, 'defaultAction'))
			{
				$ref = new ReflectionMethod(self::$controller, 'defaultAction');
				if($ref->isPublic()) 
				{
					$ref->invokeArgs(
						self::$controller,
						count(self::$path_array) > 2
							? array_slice(self::$path_array, 2)
							: array()
					);
				}
			}
			else
			{
				throw new Exception("Action ".self::$action." doesn't exist for this controller");
			}
		}
	}
	
	public static function c($key)
	{
		return isset(self::$_config[$key]) ? self::$_config[$key] : null;
	}
	
	public static function sessionStart()
	{
		if(CrushRaid::c('table_prefix') != '') {
			session_name('crushraid_'.trim(CrushRaid::c('table_prefix'), '_'));
		}
		session_start();
	}
	
	public static function userLoggedIn()
	{
		if(isset($_POST['username'], $_POST['password']))
		{
			if(self::c('admin') != trim($_POST['username']) ||
			  self::c('admin_password') != trim($_POST['password'])) {
			}
			else {
				$_SESSION['logged_in'] = true;
			}
		}
		
		return Utility::arrayValue($_SESSION, 'logged_in', false);
	}
	
	public static function userLogout()
	{
		$_SESSION['logged_in'] = false;
	}
	
	public static function assertLoggedIn()
	{
		if(!self::userLoggedIn()) {
			if(isset($_POST['username'])) {
				$message = 'Wrong credentials.';
			}
			else {
				$message = 'This page is restricted.';
			}
			$body = new View('login', compact('message'));
			
			$page = new View('page', array(
				'title' => 'CrushRaid - Login',
				'body'	=> $body
			));
			
			$page->render(true);
			exit;
		}		
	}
	
	public static function loadClass($classname)
	{
		$classname = str_replace('_', DIRECTORY_SEPARATOR, $classname);
		
		if(is_file(self::c('classes_dir').DIRECTORY_SEPARATOR.$classname.'.php'))
		{
			include_once self::c('classes_dir').DIRECTORY_SEPARATOR.$classname.'.php';
		}
		else
		{
			throw new Exception(self::c('classes_dir').DIRECTORY_SEPARATOR.$classname.'.php not found');
		}
		
		return class_exists($classname, false);
	}	
}