<?php
/**
 * Simple File caching class providing a persistent arraylike object
 * @author Christiaan Baartse <christiaan@baartse.nl>
 */
class Cache implements ArrayAccess, IteratorAggregate
{
	/**
	 * @var array Singleton instances
	 */
	protected static $_instances = array();
	
	/**
	 * @var string
	 */
	protected $_filename;
	
	/**
	 * @var array
	 */
	protected $_data;
	
	/**
	 * Get a singleton cache instance at the given filename
	 * @param string $filename
	 * @return Cache
	 */
	public static function getInstance($filename)
	{
		$filename = trim($filename);
		if(!isset(self::$_instances[$filename])) {
			self::$_instances[$filename] = new self($filename);
		}
		return self::$_instances[$filename];
	}
	
	protected function __construct($filename)
	{
		$this->_filename = CrushRaid::c('cache_dir').DIRECTORY_SEPARATOR.$filename.'.cache';
		if(file_exists($this->_filename)) {
			$data = unserialize(file_get_contents($this->_filename));
			$this->_data = is_array($data) ? $data : array();
		}
		else {
			$this->_data = array();
		}
	}
	
	/**
	 * Save the cache to disk on destruction
	 */
	public function __destruct()
	{
		if($this->_data) {
			@file_put_contents($this->_filename, serialize($this->_data));
		}
	}
	
	/**
	 * Deletes all cache entries
	 */
	public function flush()
	{
		$this->_data = array();
	}
	
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->_data);
	}

	public function offsetGet($offset)
	{
		return $this->_data[$offset];
	}

	public function offsetSet($offset, $value)
	{
		$this->_data[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->_data[$offset]);
	}
	
	public function getIterator()
	{
		return new ArrayIterator($this->_data);
	}
}