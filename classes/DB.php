<?php
class DB
{
	/**
	 * Database connection
	 * 
	 * @var mysqli
	 */
	protected static $db;
	
	public static function connect()
	{
		$details = parse_url(CrushRaid::c('dbconnection'));
		self::$db = new mysqli($details['host'], $details['user'], Utility::arrayValue($details, 'pass'), trim($details['path'], '/'));
		
		if (mysqli_connect_errno()) {
			throw new Exception("Connect failed: %s\n", mysqli_connect_error());
		}
		
		DB::query("SET NAMES 'utf8';");
	}
	
	/**
	 * Execure a query
	 * 
	 * @param string $sql
	 * @return ResultSet
	 */
	public static function query($sql)
	{
		if(!isset(self::$db)) self::connect();
		
		$res = self::$db->query($sql);
		
		if(false === $res)
		{
			throw new Exception('Query "'.$sql.'" contains errors. Error: '.self::$db->error);
		}
		
		return ($res instanceof mysqli_result) ? new ResultSet($res) : $res;
	}
	
	public static function quote($sql)
	{
		if(!isset(self::$db)) self::connect();
		
		return self::$db->real_escape_string($sql);
	}
	
	public static function getLastInsertId()
	{
		return self::$db->insert_id;
	}
}