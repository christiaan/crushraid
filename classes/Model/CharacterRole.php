<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_CharacterRole extends ORM
{
	protected $_table_name = 'character_role';
	
	protected static $_items = null;
	
	public static function registry($id = null, $load = true)
	{
		if(empty($id) || !$load)
		{
			return new self($id, $load);
		}
		
		if(is_null(self::$_items))
		{
			self::$_items = array();
			foreach (ActiveRecord::factory(__CLASS__)->get() as $item)
			{
				self::$_items[(int) $item['id']] = $item;
			}
		}
		
		return key_exists($id[0], self::$_items) ? self::$_items[$id[0]] : new self();
	}
}
