<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_Character extends ORM
{
	protected $_table_name = 'character';
	
	protected $_default_order_by = array(array('in_guild', 'DESC'), array('name', 'ASC'));

	protected $_many_to_one = array(
		'class'	=> array('Model_CharacterClass', 'class_id'),
		'race'	=> array('Model_CharacterRace', 'race_id'),
		'rank'	=> array('Model_Rank', 'rank_id'),
		'role'	=> array('Model_CharacterRole', 'role_id'),
		'offrole'	=> array('Model_CharacterRole', 'offrole_id'),
		'parent'	=> array('Model_Character', 'parent_id'),
	);
	
	protected $_one_to_many = array(
		'dkp'		=> array('Model_Dkp', 'character_id'),
		'attendees'	=> array('Model_RaidAttendee', 'character_id'),
		'loot'		=> array('Model_RaidLoot', 'character_id'),
	);
	
	/**
	 * 
	 * @param $name
	 * @param $class_id
	 * @return Model_Character
	 */
	public static function getByNameAndClassId($name, $class_id)
	{
		$character = ActiveRecord::factory('Model_Character')->
			where('name', $name)->
			where('class_id', $class_id)->
		get(true);
		
		if(!$character->isLoaded())
		{
			$character['name'] = ucfirst($name);
			$character['class_id'] = $class_id;
			$character->save();
		}
		
		return $character;
	}
	
	public function getCurrentDkp()
	{
		$id = (int) (!empty($this['parent_id']) ? $this['parent_id'] : $this['id']);
		$res = DB::query('
			SELECT SUM(`value`) AS `dkp`
			FROM `'.ActiveRecord::factory('Model_Dkp')->getTableName().'`
			WHERE character_id
			IN (
				SELECT id
				FROM `'.CrushRaid::c('table_prefix').'character`
				WHERE id = '.$id.' OR parent_id = '.$id.'
			)
		');
		return (int) $res->first()->dkp;
	}
	
	public function getAttendency($start_time = '-1 month', $end_time = null)
	{
		$start_time = is_numeric($start_time) ? $start_time : strtotime($start_time);
		$start_time = ($start_time < ($create_date = strtotime($this['create_date']))) ? $create_date : $start_time;
		$end_time = is_null($end_time) ? time() : strtotime($end_time);
		
		$res = DB::query(sprintf("
			SELECT
				SUM( UNIX_TIMESTAMP( `endtime` ) - UNIX_TIMESTAMP( `starttime` ) ) AS total_time
			FROM
				`%sraid`
			WHERE `starttime` BETWEEN '%s' AND '%s'
			AND `progress` = 1
			AND `finalized` = 1;
			",
			CrushRaid::c('table_prefix'),
			date('Y-m-d H:i:s', $start_time),
			date('Y-m-d H:i:s', $end_time)
		));
		
		$total_time = (int)$res->first()->total_time;
		
		$res = DB::query(sprintf("
			SELECT
				SUM( UNIX_TIMESTAMP( a.`endtime` ) - UNIX_TIMESTAMP( a.`starttime` ) ) AS user_time
			FROM
				`%sraid_attendee` AS a
			INNER JOIN
				`%sraid` AS r
			ON
				r.id = a.raid_id
			WHERE
				a.`character_id` = %d
			AND
				r.`starttime` BETWEEN '%s' AND '%s'
			AND r.`progress` = 1 AND r.`finalized` = 1;
			",
			CrushRaid::c('table_prefix'),
			CrushRaid::c('table_prefix'),
			$this['id'],
			date('Y-m-d H:i:s', $start_time),
			date('Y-m-d H:i:s', $end_time)
		));
		
		$user_time = (int) $res->first()->user_time;
		
		
		return (!empty($user_time) && !empty($total_time)) ? (($user_time / $total_time) * 100) : 0;
	}
	
	public function updateFromArmory($advanced = true)
	{
		if(!empty($this['name']))
		{
			$armory = Armory::getCharacter(CrushRaid::c('realm'), $this['name'], CrushRaid::c('zone'));
			if(false !== $armory)
			{
				$classes	= ActiveRecord::factory('Model_CharacterClass')->get()->getSelect('id', 'name');
				$races		= ActiveRecord::factory('Model_CharacterRace')->get()->getSelect('id', 'name');
				$this['class_id']	= (int) array_search($armory->characterInfo->character['class'], $classes);
				$this['race_id']	= (int) array_search($armory->characterInfo->character['race'], $races);
				$this['gender']		= $armory->characterInfo->character['genderId'];
				$this['in_guild']	= (int) (CrushRaid::c('guild') == $armory->characterInfo->character['guildName']);
				
				if($this['level'] < (int) $armory->characterInfo->character['level'])
				$this['level']		= (int) $armory->characterInfo->character['level'];
	
				return true;
			}
		}
		
		return false;
	}
	
	public function delete($children = true)
	{
		if($children)
		{
			$dkp = $this->dkp;
			$this->remove('dkp', $dkp);
			foreach($dkp as $v) $v->delete();
			
			$attendees = $this->attendees;
			$this->remove('attendees', $attendees);
			foreach($attendees as $v) $v->delete();
			
			$loot = $this->loot;
			$this->remove('loot', $loot);
			foreach($loot as $v) $v->delete();
		}
		
		return parent::delete();
	}
	
	public function save()
	{
		if(!$this->isLoaded()) $this['create_date'] = date('Y-m-d H:i:s');
		return parent::save();
	}
}