<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_Dkp extends ORM
{
	protected $_table_name = 'dkp';
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by = array('id', 'DESC');
	
	public function save()
	{
		$this['datetime'] = date('Y-m-d H:i:s');
		return parent::save();
	}
	
	public static function recalculateDkp($char_id = null)
	{
		DB::query(sprintf('
			UPDATE
				%1$scharacter AS c,
				(SELECT
				c2.id,
				SUM(d.value) AS value
				FROM %1$scharacter AS c2
				LEFT JOIN
				%1$sdkp AS d
				ON
				d.character_id IN (SELECT c3.id FROM %1$scharacter AS c3 WHERE c3.id = COALESCE(c2.parent_id, c2.id) OR parent_id = COALESCE(c2.parent_id, c2.id))
				GROUP BY c2.id) AS dkp
			SET
				c.dkp = dkp.value
			WHERE
				dkp.id = c.id
			%2$s
			',
			CrushRaid::c('table_prefix'),
			is_null($char_id) ? '' : 'AND c.id = '.(int) $char_id
		));
		
		return true;
	}
}