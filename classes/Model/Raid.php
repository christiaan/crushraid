<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_Raid extends ORM
{
	protected $_table_name = 'raid';
	
	protected $_default_order_by = array('starttime', 'DESC');
	
	protected $_one_to_many = array(
		'attendees' => array('Model_RaidAttendee', 'raid_id'),
		'loot' => array('Model_RaidLoot', 'raid_id'),
		'events' => array('Model_RaidEvent', 'raid_id'),
	);
	
	protected $_many_to_one = array(
		'instance' => array('Model_Instance', 'instance_id'),
	);
	
	protected $_attendees;
	
	/**
	 * Get the attendees in the raid sorted by Character name
	 * 
	 * @return array
	 */
	public function getAttendees()
	{
		if(!isset($this->_attendees))
		{
			$attendees_sorted = array();
			$this->_attendees = array();
			foreach ($this->attendees as $k => $attendee) $attendees_sorted[$k] = $attendee->character['name'];
			natcasesort($attendees_sorted);
			foreach ($attendees_sorted as $key => $name) $this->_attendees[] = $this->attendees[$key];
		}
		
		return $this->_attendees;
	}
	
	protected $_characters;
	
	/**
	 * Get a list of Characters in the raid
	 * 
	 * @return ActiveRecordIterator
	 */
	public function getCharacters()
	{
		if(!($this->_characters instanceof ActiveRecordIterator))
		{
			$res = DB::query(sprintf("
				SELECT DISTINCT
					c.*
				FROM
					`%sraid_attendee` AS ra
				INNER JOIN
					`%scharacter` AS c
				ON
					c.`id` = character_id
				WHERE
					raid_id = %d
				ORDER BY
					c.`name` ASC
				",
				CrushRaid::c('table_prefix'),
				CrushRaid::c('table_prefix'),
				$this['id']
			));
			
			$this->_characters = new ActiveRecordIterator('Model_Character', $res);
		}
		
		return $this->_characters;
	}
	
	/**
	 * Add a looted item to the raid
	 * 
	 * @param Model_Character $character
	 * @param strin $itemname
	 * @param int $cost
	 * @param string $datetime
	 * @return Model_RaidLoot
	 */
	public function addLoot($character, $itemname, $cost = 0, $datetime = null, $note = '', $itemid = 0)
	{
		if(!empty($itemname))
		{
			$new_dkp = ActiveRecord::factory('Model_Dkp');
			$new_dkp['character_id'] = ($character instanceof Model_Character) ? $character['id'] : $character;
			$new_dkp['value'] = 0 - $cost;
			$new_dkp['note'] = $itemname;
			$new_dkp->save();
			
			$new_loot = ActiveRecord::factory('Model_RaidLoot');
			$new_loot['raid_id'] = $this['id'];
			$new_loot['character_id'] = ($character instanceof Model_Character) ? $character['id'] : $character;
			$new_loot['dkp_id'] = $new_dkp['id'];
			$item = ActiveRecord::factory('Model_Item')->getItemByName($itemname);
			if(!empty($itemid))
			{
				$item['wowid'] = $itemid;
				$item->save();
			}
			$new_loot['item_id'] = $item['id'];
			$new_loot['datetime'] = !empty($datetime) ? $datetime : date('Y-m-d H:i:s');
			$new_loot['note'] = $note;
			$new_loot->save();
			
			return $new_loot;
		}
		
		return false;
	}
	
	/**
	 * Add an Attendee to the raid
	 * 
	 * @param Model_Character $character
	 * @param string $starttime
	 * @param string $endtime
	 * @param bool $standby
	 * @return Model_RaidAttendee
	 */
	public function addAttendee($character, $starttime, $endtime = null, $standby = false, $note = '')
	{
		$new_attendee = ActiveRecord::factory('Model_RaidAttendee');
		$new_attendee['raid_id'] = $this['id'];
		$new_attendee['character_id'] = ($character instanceof Model_Character) ? $character['id'] : $character;
		$new_attendee['starttime'] = !empty($starttime) ? $starttime : $this['starttime'];
		$new_attendee['endtime'] = $endtime;
		$new_attendee['standby'] = (int) $standby;
		$new_attendee['note'] = $note;
		$new_attendee->save();
		
		return $new_attendee;
	}
	
	
	/**
	 * @param $name
	 * @param $datetime
	 * @param $dkp
	 * @return Model_RaidEvent
	 */
	public function addEvent($name, $datetime, $dkp)
	{
		$new_event = ActiveRecord::factory('Model_RaidEvent');
		$new_event['raid_id'] = $this['id'];
		$new_event['name'] = $name;
		$new_event['datetime'] = $datetime;
		$new_event['dkp'] = $dkp;
		$new_event->save();
		
		return $new_event;
	}
	
	/**
	 * Fill a raid using a headcount XML export
	 * 
	 * @param SimpleXMLElement $headcount
	 * @return bool
	 */
	public function importHeadcountXml(SimpleXMLElement $headcount)
	{
		$classes = ActiveRecord::factory('Model_CharacterClass')->get()->getSelect('id', 'name');
		$this['starttime']	= date('Y-m-d H:i:s', strtotime($headcount->start));
		$this['endtime']	= date('Y-m-d H:i:s', strtotime($headcount->end));
		$this->save();
		
		// Create a charactername -> id lookup array
		$chars = array();
		foreach($headcount->players->player as $player) {
			$char = Model_Character::getByNameAndClassId($player->name, (int) array_search($player->class, $classes));
			$chars[$char['id']] = ucfirst($char['name']);
			
			$entries = array();
			foreach($player->attendance->event as $event)
			{
				$entries[] = array(
					'standby'		=> (FALSE === stripos($event->note, 'raid list')), // TODO Fix this please mister Headcount!
					'starttime'		=> strtotime($event->start),
					'endtime'		=> strtotime($event->end),
					'note'			=> $event->note,
				);
			}
			
			$sorted_keys = array();
			foreach($entries as $k => $entry)
			{
				$sorted_keys[$k] = $entry['starttime'];
			}
			sort($sorted_keys, SORT_NUMERIC);
			$sorted_keys = array_keys($sorted_keys);
			
			// Remove small time entries
			for($i=count($sorted_keys)-1; $i>0; $i--)
			{
				$cur_key	= $sorted_keys[$i];
				$prev_key	= $sorted_keys[($i-1)];
				if(
					($entries[$cur_key]['endtime'] - $entries[$cur_key]['starttime']) < (2*60)
				)
				{
					unset($entries[$cur_key]);
					unset($sorted_keys[$i]);
				}
			}
			
			$sorted_keys = array_slice($sorted_keys, 0);
			
			// Merge time entries
			for($i=count($sorted_keys)-1; $i>0; $i--)
			{
				$cur_key	= $sorted_keys[$i];
				$prev_key	= $sorted_keys[($i-1)];
				if(
					$entries[$cur_key]['standby'] == $entries[$prev_key]['standby'] &&
					$entries[$cur_key]['starttime'] < $entries[$prev_key]['endtime'] + (3*60)
				)
				{
					// Merge the attendency records
					$entries[$prev_key]['endtime'] = $entries[$cur_key]['endtime'];
					unset($entries[$cur_key]);
					unset($sorted_keys[$i]);
				}
			}
			
			foreach($entries as $entry)
			{
				$this->addAttendee(
					$char,
					date('Y-m-d H:i:s', $entry['starttime']),
					date('Y-m-d H:i:s', $entry['endtime']),
					$entry['standby'],
					$entry['note']
				);
			}
		}
		
		foreach($headcount->bossKills->boss as $boss)
		{
			$this->addEvent($boss->name, date('Y-m-d H:i:s', strtotime($boss->time)), 0);						
		}
		
		foreach($headcount->loot->item as $loot)
		{
			$this->addLoot(
				(int) array_search($loot->looter, $chars),
				$loot->name,
				$loot->cost,
				date('Y-m-d H:i:s', strtotime($loot->time)),
				$loot->looter.', Note: '.$loot->note,
				$loot->id
			);						
		}
		
		return true;
	}
	
	/**
	 * Finalize a running raid
	 * 
	 * @param $dkp_per_hour
	 * @param $standby_per_hour
	 * @param $refinalize
	 * @return bool
	 */
	public function finalize($dkp_per_hour = 2, $standby_per_hour = 2, $refinalize = false)
	{
		$dkp_per_minute = $dkp_per_hour / 60;
		$standby_per_minute = $standby_per_hour / 60;
		
		if($refinalize || !$this['finalized'])
		{
			try
			{
				if(empty($this['endtime'])) $this['endtime'] = date('Y-m-d H:i:s');
				$this['finalized'] = 1;
				$this->save(); // Lock it for other officers
			
				// Set all open attendee endtimes to the endtime of the raid
				if( ! DB::query(sprintf('
					UPDATE `%sraid_attendee`
					SET `endtime` = "%s"
					WHERE `raid_id` = %d
					AND `endtime` = 0
					',
					CrushRaid::c('table_prefix'),
					$this['endtime'],
					$this['id']
				)))
				{
					throw new Exception('Failed to finalize raid, couldn\'t close Attendee endtimes');
				}
				
				// Delete all previous created event dkp
				foreach($this->events as $event)
				{
					$event->removeAttendees();
				}
				
				// Award the timed dkp and event dkp
				foreach($this->attendees as $attendee)
				{
					$dkp = empty($attendee['dkp_id']) ? ActiveRecord::factory('Model_Dkp') : ActiveRecord::factory('Model_Dkp', $attendee['dkp_id']);
					$dkp['character_id'] = $attendee['character_id'];
					$minutes_in_raid = ceil((strtotime($attendee['endtime']) - strtotime($attendee['starttime'])) / 60);
					
					if($attendee['standby'])
					{
						$dkp['value'] = round($minutes_in_raid * $standby_per_minute);
						$dkp['note'] = sprintf(
							"Dkp for raid standby:%s %s",
							$this['title'],
							strftime("%d %b %y", strtotime($this['starttime']))
						);
					}
					else
					{						
						$dkp['value'] = round($minutes_in_raid * $dkp_per_minute);
						$dkp['note'] = sprintf(
							"Dkp for raid attendency:%s %s",
							$this['title'],
							strftime("%d %b %y", strtotime($this['starttime']))
						);
						
						foreach($this->events as $event)
						{
							$eventtime = strtotime($event['datetime']);
							
							if($eventtime >= strtotime($attendee['starttime']) && $eventtime <= strtotime($attendee['endtime']))
							{
								$event->addAttendee($attendee);
							}
						}
					}
					
					$dkp->save();
					$attendee['dkp_id'] = $dkp['id'];
					$attendee->save();
				}

				Model_Dkp::recalculateDkp();
				return true;
			}
			catch(Exception $e)
			{
				$this['finalized'] = 0;
				$this->save(); // Lock it for other officers
				throw $e;
			}
		}
		
		return false;
	}
	
	public function delete($children = true)
	{
		if($children)
		{
			$attendees = $this->attendees;
			$this->remove('attendees', $attendees);
			foreach($attendees as $child) $child->delete();
			
			$loot = $this->loot;
			$this->remove('loot', $loot);
			foreach($loot as $child) $child->delete();
		}
		
		return parent::delete();
	}
}
