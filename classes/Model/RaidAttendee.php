<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_RaidAttendee extends ORM
{
	protected $_table_name = 'raid_attendee';

	protected $_default_order_by = array('starttime', 'DESC');
	
	/**
	 * Many to one configuration (this->1 / parent)
	 *
	 * @var array
	 */
	protected $_many_to_one = array(
		'raid' => array('Model_Raid', 'raid_id'),
		'character' => array('Model_Character', 'character_id'),
		'dkp' => array('Model_Dkp', 'dkp_id'),
	);
	
	public function delete($childer = true)
	{
		$this->dkp->delete();
		
		return parent::delete();
	}
}