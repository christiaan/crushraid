<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Model_RaidEvent extends ORM
{
	protected $_table_name = 'raid_event';

	protected $_default_order_by = array('datetime', 'ASC');
	
	protected $_many_to_one = array(
		'raid' => array('Model_Raid', 'raid_id'),
	);
	
	protected $_many_to_many = array(
		'attendees' => array('Model_RaidAttendee', 'raid_attendee_event', 'event_id', 'attendee_id'),
	);
	
	public function addAttendee(Model_RaidAttendee $attendee)
	{
		if(!$this->isLoaded() || !$attendee->isLoaded())
		{
			throw new Exception('Couldn\'t add a nonexistant attendee to a nonexistant event');
		}
		$res = DB::query(sprintf("
			SELECT dkp_id
			FROM `%sraid_attendee_event` AS rae
			WHERE attendee_id = %d
			AND event_id = %d
			",
			CrushRaid::c('table_prefix'),
			$attendee['id'],
			$this['id']
		));
		
		$dkp = ActiveRecord::factory('Model_Dkp', count($res) ? $res->first()->dkp_id : null);
		$dkp['value'] = $this['dkp'];
		$dkp['note'] = 'Dkp for raid event: '.$this['name'];
		$dkp['character_id'] = $attendee['character_id'];
		$dkp->save();
		
		DB::query(sprintf("
			REPLACE INTO `%sraid_attendee_event`
			(`attendee_id`, `event_id`, `dkp_id`)
			VALUES
			(%d, %d, %d)
			",
			CrushRaid::c('table_prefix'),
			$attendee['id'],
			$this['id'],
			$dkp['id']
		));
		
		return true;
	}
	
	public function removeAttendees()
	{
		if(!$this->isLoaded()) return false;
		
		$res = DB::query(sprintf("
			SELECT dkp_id
			FROM `%sraid_attendee_event` AS rae
			WHERE event_id = %d
			",
			CrushRaid::c('table_prefix'),
			$this['id']
		));
		
		foreach($res as $dkp_id)
		{
			ActiveRecord::factory('Model_Dkp', $dkp_id->dkp_id)->delete();
		}
		
		DB::query(sprintf("
			DELETE FROM `%sraid_attendee_event`
			WHERE event_id = %d
			",
			CrushRaid::c('table_prefix'),
			$this['id']
		));
		
		return true;
	}
}