<?php
/**
 * Active Record
 * 
 * @author Christiaan Baartse <christiaan@baartse.nl>
 */
abstract class ActiveRecord implements ArrayAccess
{
	/**
	 * Array holding the values of the current record
	 *
	 * @var array
	 */
	protected $_values;
	
	/**
	 * MD5 has of the current values
	 *
	 * @var array
	 */
	protected $_values_hash;
	
	/**
	 * The pk of the currently loaded record
	 *
	 * @var array
	 */
	protected $_loaded_pk;
	
	/**
	 * Name of the database Table
	 *
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * The order by to use as default
	 *
	 * @var array
	 */
	protected $_default_order_by;
	
	/**
	 * Array used for storing the query parameters
	 *
	 * @var array
	 */
	protected $_query_parts = array();

	/**
	 * The last query that ran
	 *
	 * @var string
	 */
	protected $_last_query;
	
	/**
	 * Cache for this table's properties
	 * 
	 * @var array
	 */
	protected $_local_table;
	
	/**
	 * Factory method
	 *
	 * @throws Exception when the $classname is no ActiveRecord
	 * @param integer $id
	 * @param boolean $load Auto load the record in the object
	 * @return ActiveRecord
	 */
	public static function factory($classname, $id = null, $load = true)
	{
		if($load !== false && !is_null($id))
		{
			if($load !== true && !is_array($id))
			{ // The $load is something else then a Bool!
				$id = func_get_args();
				$classname = array_shift($id);
				$load = true;
			}
		}
		
		if(!is_subclass_of($classname, __CLASS__))
		{
			throw new Exception($classname.' is no '.__CLASS__);
		}
		
		// If the class has a registry method use that to instantiate a new object
		// so we can implement a registry for some classes
		if(method_exists($classname, 'registry')) {
			return call_user_func(array($classname, 'registry'), $id, $load);
		} else {
			return new $classname($id, $load);
		}
	}
	
	/**
	 * Magic Construct
	 *
	 * @param string $id
	 * @param bool $load
	 */
	public function __construct($id = null, $load = true)
	{
		$this->_loadTableDefinition();
		$this->getTableName();
		$this->flushValues();
		
		if($load !== false && !is_null($id)) {
			if($load !== true && !is_array($id)) { // The $load is something else then a Bool!
				$id = func_get_args();
			}
			$this->get($id);
		}
	}
	
	/**
	 * Magic getter for the record values
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function offsetGet($name)
	{
		return $this->offsetGetReal($name);
	}
	
	/**
	 * This function should always return the real value as in the database
	 *
	 * @throws Exception
	 * @param string $name
	 * @return mixedvar
	 */
	public function offsetGetReal($name)
	{
		if( isset($this->_local_table['columns'][$name]) )
		{
			return array_key_exists($name, $this->_values) ? $this->_values[$name] : null;
		}
		else
		{
			throw new Exception(get_class($this)." has no column named ".$name."!");
		}
	}
	
	/**
	 * Magic setter for the record values
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function offsetSet($name, $value)
	{
		$this->offsetSetReal($name, $value);
	}
	
	/**
	 * This function should always be set to set the real db value
	 *
	 * @param string $name
	 * @param mixedvar $value
	 */
	public function offsetSetReal($name, $value)
	{
		if( isset($this->_local_table['columns'][$name]) )
		{
			$this->_values[$name] = $value;
		}
		else
		{
			throw new Exception(get_class($this)." has no column named ".$name."!");
		}
	}
	
	/**
	 * Magic isset for the record values
	 *
	 * @param string $name
	 * @return bool
	 */
	public function offsetExists($name)
	{
		return isset($this->_local_table['columns'][$name]) && array_key_exists($name, $this->_values);
	}
	
	/**
	 * Magic unset for the record values
	 *
	 * @param string $name
	 */
	public function offsetUnset($name)
	{
		if($this->offsetExists($name)) {
			unset($this->_values[$name]);
		}
	}
	
	/**
	 * Reset the values
	 */
	public function flushValues()
	{
		$this->_loaded_pk = array();
		$this->_values = array();
	}
	
	/**
	 * Create ActiveRecords from DB results
	 *
	 * @param objec/array $result
	 * @param string $className
	 * @return ActiveRecord
	 */
	public function loadResult($result, $className = null)
	{
		if(is_array($result) && (is_object(current($result)) || is_array(current($result)))) {
			// Record Set
			
			if(is_null($className)){
				$className = get_class($this);
			}
			$objects = array();
			foreach($result as $row) {
				$object = ActiveRecord::factory($className);
				$object->loadResult($row);
				$objects[] = $object;
			}
			return (array) $objects;
		} else {
			// Single Record
			
			$this->flushValues(); // Clean up the old variables
			if(is_object($result))
			{
				$result = get_object_vars($result);
			}
			
			if(!empty($result))
			{
				foreach($result as $key => $value)
				{
					$this->offsetSetReal($key, $value);
				}
				$this->_loaded_pk = $this->getPk();
				$this->_values_hash = $this->getCurrentValuesHash();
				return $this;
			}
		}
	}
	
	/**
	 * Fetch the records
	 *
	 * @return ActiveRecord
	 */
	public function get($id = null)
	{
		$columns = $this->getColumnsSql();
		if(!empty($columns)) {
			if(is_null($id) || $id === TRUE) {
				// Save the queryparts before we use them, this way we can do a count(*) afterwards
				$this->_last_query = $this->_query_parts;
				// Query!
				$sql = sprintf("
					SELECT
						%s
					FROM
						`%s`
					%s
					%s
					%s
				",
				$columns,
				DB::quote($this->getTableName()),
				$this->sqlWhere(),
				$this->sqlOrderBy(),
				$this->sqlLimit()
				);
				
				$res = DB::query($sql);
				
				if($id === TRUE) {
					return (count($res) > 0) ? $this->loadResult($res->first()) : $this;
				} else {
					return new ActiveRecordIterator(get_class($this), $res);
				}
			} else {
				// Fetch a record by PK
				if(!is_array($id)) $id = func_get_args();
				return $this->wherePk($id)->limit(1)->get(TRUE);
			}
		}
	}
	
	/**
	 * Same as get() only gives back nr of rows
	 * 
	 * @param bool $last_query Count the results of the last query that ran
	 * @return integer
	 */
	public function getCount($last_query = true)
	{
		if($last_query && !empty($this->_last_query)) {
			$this->_query_parts = $this->_last_query;
		}
		$sql = sprintf("
			SELECT
				COUNT(*) as cnt
			FROM (
				SELECT
					%s
				FROM
					%s
				%s
			) as lastQuery
			",
			$this->getColumnsSql(),
			DB::quote($this->getTableName()),
			$this->sqlWhere()
		);
		
		$res = DB::query($sql);
		
		return (int) $res->first()->cnt;		
	}
	
	/**
	 * Save the record
	 *
	 * @throws Exception
	 * @return boolean Successfully saved
	 */
	public function save()
	{
		$table_name = $this->getTableName();
		$new_values_hash = $this->getCurrentValuesHash();
		$values = array();
		$auto_increment_column = '';
		foreach($this->_local_table['columns'] as $col_name => $column)
		{
			// If its an updatable field and its changed add it it to the update arrays
			if($column['auto_increment'] && null === $this->offsetGetReal($col_name))
			{
				$auto_increment_column = $col_name;
			}
			
			if(
				!isset($this->_values_hash[$col_name]) ||
				(isset($this->_values_hash[$col_name]) && $new_values_hash[$col_name] != $this->_values_hash[$col_name])
			)
			{
				if(is_null($this->offsetGetReal($col_name)))
				{
					$values[$col_name] = ($column['null']) ? 'NULL' : "''";
				}
				else
				{
					$values[$col_name] = "'".DB::quote($this->offsetGetReal($col_name))."'";
				}
			}
		}
		
		if($this->isLoaded())
		{
			if(count($values) != 0)
			{
				// Updating an existing record
				$update_sql = '';
				foreach($values as $column => $value)
				{
					if($update_sql != '') $update_sql .= ', ';
					$update_sql .= sprintf("`%s` = %s", $column, $value);
				}
				
				$this->sqlWhere(); //Flush the wherepart
				$this->wherePk(); //Always match on the current PK
				
				$sql = sprintf(
					"UPDATE
					`%s`
					SET
					%s
					%s
					LIMIT 1;",
					DB::quote($table_name),
					$update_sql,
					$this->sqlWhere()
				);
				$res = DB::query($sql);
				
				$this->_values_hash = $this->getCurrentValuesHash();
				$this->_loaded_pk = $this->getPk();
				
				return true;
			}
		}
		else
		{
			if(count($values) != 0)
			{
				// Insert
				$insert_columns = "`".implode("`, `", array_keys($values))."`";
				$insert_values = implode(", ", $values);
				$sql = sprintf("
					INSERT INTO
						`%s`
						(%s)
					VALUES
						(%s)
					",
					DB::quote($table_name),
					$insert_columns,
					$insert_values
				);
				$res = DB::query($sql);
				
				if(!empty($auto_increment_column))
				{
					$this->offsetSetReal($auto_increment_column, DB::getLastInsertId());
				}
				$this->_values_hash = $this->getCurrentValuesHash();
				$this->_loaded_pk = $this->getPk();
				
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Delete the current record
	 *
	 * @throws Exception
	 * @return boolean Successfully deleted
	 */
	public function delete()
	{
		if($this->isLoaded())
		{
			$this->sqlWhere(); //Flush the wherepart
			$this->wherePk()->limit(1);
			
			$sql = sprintf("
					DELETE
					FROM `%s`
					%s
					%s",
				DB::quote($this->getTableName()),
				$this->sqlWhere(),
				$this->sqlLimit()
			);
			
			$res = DB::query($sql);
			$this->flushValues();
			return false;
		}
		return false;
	}
	
	/**
	 * Get the values of the current Primary Key
	 *
	 * @return array
	 */
	public function getPk()
	{
		$pk = array();
		foreach($this->_local_table['primary_key'] as $col_name) {
			$pk[] = $this->offsetGetReal($col_name);
		}
		return $pk;
	}
	
	/**
	 * Get the where clausule to match the PK
	 *
	 * @param string $id If none is given uses the current PK
	 * @return ActiveRecord Self
	 */
	public function wherePk($id = null)
	{
		if(is_null($id)) {
			$id = !empty($this->_loaded_pk) ? $this->_loaded_pk : $this->getPk();
		} else if(!is_array($id)) {
			$id = func_get_args();
		}
		$where = array();
		$pk    = $this->_local_table['primary_key'];
		
		if(count($pk) != count($id))
		{
			throw new Exception(get_class($this)."'s Primary Key consists of ".count($pk)." columns. ".count($id)." keys found.");
		}
		foreach($pk as $col_name)
		{
			$where[$col_name] = array_shift($id);
		}
		return $this->where($where);
	}
	
	/**
	 * Generate a unique hash for the current columns 
	 * and values so we can see if the record has changed
	 *
	 * @return string
	 */
	public function getCurrentValuesHash()
	{
		return array_map('md5', $this->getAsArray());
	}
	
	/**
	 * Did the record change since the last loadResult() or instantiation
	 *
	 * @return bool
	 */
	public function isAltered()
	{
		return ($this->_values_hash != $this->getCurrentValuesHash());
	}
	
	/**
	 * Do we got a record
	 *
	 * @return bool
	 */
	public function isLoaded()
	{
		return (count($this->_loaded_pk) != 0 && !is_null(max($this->_loaded_pk)));
	}
	
	/**
	 * Get the current column values
	 *
	 * @return array
	 */
	public function getAsArray()
	{
		$values = array();
		foreach($this->_local_table['columns'] as $col_name => $column) {
			$values[$col_name] = $this->offsetGetReal($col_name);
		}
		return $values;
	}
	
	/**
	 * Add where statement
	 *
	 * @param string $key Colum name
	 * @param string $value
	 * @param string $operator
	 * @param bool $dbquote Quote the value
	 * @return ActiveRecord
	 */
	public function where($key, $value = null, $operator = '=', $dbquote = TRUE)
	{
		$keys = is_array($key) ? $key : array($key => $value);
		
		foreach($keys as $key => $value) {
			$this->_query_parts['where'][] = array($key, $value, 'AND', $operator, $dbquote);
		}
		
		return $this;
	}
	
	/**
	 * Add where statement
	 *
	 * @param string $key Colum name
	 * @param string $value
	 * @param string $operator
	 * @param bool $dbquote Quote the value
	 * @return ActiveRecord
	 */
	public function orWhere($key, $value = null, $operator = '=', $dbquote = TRUE)
	{
		$keys = is_array($key) ? $key : array($key => $value);
		
		if(!is_array($this->_query_parts['where'])) $this->_query_parts['where'] = array();
		foreach($keys as $key => $value) {
			$this->_query_parts['where'][] = array($key, $value, 'OR', $operator, $dbquote);
		}
		
		return $this;
	}
	
	/**
	 * Custom Where clausule
	 *
	 * @param string $sql
	 * @return ActiveRecord
	 */
	public function customWhere($sql = null) {
		if(is_null($sql)){
			return $this->_where_custom;
		}
		$this->_query_parts['where_custom'] = $sql;
		return $this;
	}
	
	/**
	 * Where the given column matches one of the array values
	 *
	 * @param string $key Colum name
	 * @param array $values
	 * @param bool $not_in use NOT IN instead of IN
	 * @return ActiveRecord
	 */
	public function whereIn($key, array $values, $not_in = false)
	{
		$vals = array();
		foreach ($values as $val)
		{
			$vals[] = 
				is_null($val) ? 'NULL' : 
				(is_numeric($val) ? $val :
				"'".DB::quote($val)."'");
		}
		
		return empty($values) ? $this : $this->where($key, '('.implode(', ', $vals).')', $not_in ? 'NOT IN' : 'IN', false);		
	}
	
	/**
	 * Where the given column does NOT matches one of the array values
	 *
	 * @param string $key Colum name
	 * @param array $values
	 * @return ActiveRecord
	 */
	public function whereNotIn($key, array $values)
	{
		return $this->whereIn($key, $values, true);
	}
	
	/**
	 * Create the sql for the where statements
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlWhere($flush = true)
	{
		$clausule = '';
		$clausule.= (!empty($this->_query_parts['where_custom']))? $this->_query_parts['where_custom'] : '';
		
		if(!empty($this->_query_parts['where'])){
			foreach($this->_query_parts['where'] as $where){
				list($key, $value, $and_or, $operator, $dbquote) = $where;
				if($clausule != '') {
					$clausule.= ' '.$and_or.' ';
				}
				$key = $dbquote ? "`".$key."`" : $key;
				$value = $dbquote ? "'".DB::quote($value)."'" : $value;
				$clausule .= "{$key} {$operator} {$value}";
			}
		}
		if($flush) {
			$this->_query_parts['where_custom'] = '';
			$this->_query_parts['where'] = array();
		}
		if($clausule != ''){$clausule = "WHERE\n".$clausule;}
		return $clausule;
	}
	
	/**
	 * Limit the query
	 *
	 * @param integer $items
	 * @param integer $offset
	 * @return ActiveRecord
	 */
	public function limit($items = null, $offset = null){
		if(is_null($items) && is_null($offset)){
			$this->_query_parts['limit'] = '';
			return $this;
		}
		if(!is_null($items) && is_null($offset)){
			$this->_query_parts['limit'] = "LIMIT {$items}";
			return $this;
		}
		
		$this->_query_parts['limit'] = "LIMIT {$offset}, {$items}";
		return $this;
	}
	
	/**
	 * Output the sql for the limit
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlLimit($flush = true){
		$limit = !empty($this->_query_parts['limit']) ? $this->_query_parts['limit'] : '';
		if($flush) {
			$this->_query_parts['limit'] = '';
		}
		return $limit;
	}
	
	/**
	 * Order by
	 *
	 * @param string $key
	 * @param string $order
	 * @return ActiveRecord
	 */
	public function orderBy($key, $order = 'ASC'){
		if(!is_string($key) || !is_string($order)) {
			throw new Exception(__METHOD__." expects a string as parameter");
		}
		if(!isset($this->_query_parts['order_by']) || !is_array($this->_query_parts['order_by'])) $this->_query_parts['order_by'] = array();
		$this->_query_parts['order_by'][] = array($key, $order);
		return $this;
	}
	
	/**
	 * Output the sql for the Order By
	 *
	 * @param bool $flush Reset the query parts
	 * @return string
	 */
	protected function sqlOrderBy($flush = true){
		$order_by = '';
		$order_by_array = array();
		if(!empty($this->_query_parts['order_by'])) {
			$order_by_array = $this->_query_parts['order_by'];
		} else if(!empty($this->_default_order_by) && is_array($this->_default_order_by)) {
			if(!is_array(current($this->_default_order_by))) $order_by_array = array($this->_default_order_by); 
			else $order_by_array = $this->_default_order_by;			
		}
		
		
		foreach($order_by_array as $ob) {
			if($order_by != '') $order_by.= ', ';
			list($key, $order) = $ob;
			$order_by.= sprintf("`%s` %s\n", $key, $order);
		}		
		
		if($flush) {
			$this->_query_parts['order_by'] = array();
		}
		if($order_by != ''){$order_by = "ORDER BY\n".$order_by;}
		return $order_by;
	}
	
	/**
	 * The columns escaped as SQL select part
	 *
	 * @return string
	 */
	public function getColumnsSql($table_name = null)
	{
		$columns = $this->_local_table['columns'];
		if(is_array($columns)) {
			if(!is_null($table_name)) {
				$table_name = DB::quote($table_name);
				return "`{$table_name}`.`".implode("`,\n `{$table_name}`.`", array_keys($columns))."`";
			} else {
				return "`".implode("`,\n `", array_keys($columns))."`";
			}
		}
	}
	
	/**
	 * Get the table properties
	 *
	 * @return array/array_key
	 */
	public function getTableProperty($property = null)
	{
		if (is_null($property)) {
			return $this->_local_table;
		}
		else if (isset( $this->_local_table[$property])) {
			return $this->_local_table[$property];
		}
		else {
			throw new InvalidArgumentException( sprintf("Illegal argument for call: %s",$property));
		} 	
	}
	
	/**
	 * Get the Tablename for the current class
	 *
	 * @return string
	 */
	public function getTableName()
	{
		if(!isset($this->_table_name)) {
			// Camelcase to underscored
			$this->_table_name = strtolower(trim(ereg_replace("[A-Z]","_\\0", str_replace("_", '', get_class($this))), '_'));
		}

		return CrushRaid::c('table_prefix').$this->_table_name;
	}
	
	protected function _loadTableDefinition()
	{
		$table_name = $this->getTableName();
		
		$cache = Cache::getInstance('activeRecords');
		
		if(!isset($cache[$table_name])) 
		{
			$res = DB::query(sprintf("DESC `%s`", DB::quote($table_name)));
			$primary_key = array();
			$columns = array();
			foreach($res as $column) {
				if($column->Key == 'PRI') {
					$primary_key[] = $column->Field;
				}
				
				$columns[$column->Field] = array(
					'type' => sscanf(str_replace(str_split("()"), " ", $column->Type), '%s %s %s'),
					'null' => $column->Null == 'YES',
					'default' => $column->Default,
					'auto_increment' => $column->Extra == 'auto_increment',
					'primary_key' => $column->Key == 'PRI',
					'unique' => $column->Key == 'UNI'
				);
			}
			
			$cache[$table_name] = array(
				'primary_key' => $primary_key,
				'columns' => $columns
			);
		}
		
		$this->_local_table = $cache[$table_name]; 
		return true;
	}
}
