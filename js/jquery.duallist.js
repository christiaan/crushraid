;(function($){
	
	$.fn.duallist = function(options) {
		var opt = $.extend(true, {}, $.fn.duallist.defaults, options);
		
		return this.each(function(){
			var $this = $(this),
				duallist = $(opt.template);
			
			duallist.insertAfter($this);
			$this.change(function(e){
				var selected = [],
				deselected = [];
				$this.find('option').each(function(i){
					if($(this).is(':selected')) {
						selected.push($('<option></option>').attr('value', i).html($(this).html()));
					} else {
						deselected.push($('<option></option>').attr('value', i).html($(this).html()));
					}
				});
				
				$('.duallist_selected select', duallist).empty();
				$.each(selected, function(){
					$('.duallist_selected select', duallist).append(this);
				});
				$('.duallist_deselected select', duallist).empty();
				$.each(deselected, function(){
					$('.duallist_deselected select', duallist).append(this);
				});
			}).change();
			
			if(opt.autosize) {
				var height = Math.floor(($this.height() - $('.duallist_buttons', duallist).height()) / 2);
				$('.duallist_selected select, .duallist_deselected select', duallist).css('height', height+'px');
			}
			
			$('.duallist_select', duallist).click(select);
			$('.duallist_deselected select', duallist).dblclick(select);
			
			$('.duallist_deselect', duallist).click(deselect);
			$('.duallist_selected select', duallist).dblclick(deselect);
			
			$this.hide();
			
			function select() {
				$('.duallist_deselected select option:selected', duallist).each(function(){
					$this.find('option:eq('+$(this).attr('value')+')').attr('selected', true);
				});
				$this.change();
				
				return false;
			};
			
			function deselect() {
				$('.duallist_selected select option:selected', duallist).each(function(){
					$this.find('option:eq('+$(this).attr('value')+')').attr('selected', false);
				});
				$this.change();
				
				return false;
			};
		});
	};
	
	$.fn.duallist.counter = 0;
	
	$.fn.duallist.defaults = {
		template : 
			'<div class="duallist">'+
			'	<div class="duallist_deselected"><select multiple="multiple"></select></div>'+
			'	<div class="duallist_buttons"><input class="duallist_select" type="button" value="Add" />&nbsp;<input class="duallist_deselect" type="button" value="Remove" /></div>'+
			'	<div class="duallist_selected"><select multiple="multiple"></select></div>'+
			'</div>',
		autosize : true
	};
	
	
})(jQuery);
