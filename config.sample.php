<?php
// Fill in the entries and rename to config.php
return array(
	'homepage_url' => 'http://mysth.wowstead.com/',
	'realm' => 'Outland', // Realm
	'guild' => 'Mysth', // What else, your guild's name!
	'zone' => 'eu', // Either eu or us
	'level' => '80', // Minimum level for armory characters to get imported
	'timezone' => 'Europe/Amsterdam', // For valid values see http://php.net/manual/en/timezones.php
	'dbconnection' => 'mysql://username:password@localhost/crushraid',
	'admin' => 'admin', // Username for the admin interface
	'admin_password' => 'admin', // Password for the admin interface
	'add_from_armory' => false, // Add new characters from the armory or just update existing ones
	'baseurl' => '/crushraid/index.php', // Used to create urls needs trailing index.php if you don't got mod_rewrite set up
	'basepath' => '/crushraid/', // Used to find resources like pictures
	'default_controller' => 'Character',
	'table_prefix' => 'crushraid_', // Table prefix
	'classes_dir' => dirname(__FILE__).DIRECTORY_SEPARATOR.'classes',
	'views_dir' => dirname(__FILE__).DIRECTORY_SEPARATOR.'views',
	'cache_dir' => dirname(__FILE__).DIRECTORY_SEPARATOR.'cache',
);