<?php
/*
 * Copyright 2009 Christiaan Baartse <christiaan@baartse.nl>
 * 
 * This file is part of Crushraid
 * 
 * Crushraid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
try {
	if(file_exists('config.php')) {
		$config = require 'config.php';
	}
	else {
		die('Configuration missing');
	}
	date_default_timezone_set($config['timezone']);
	header ('Content-type: text/html; charset=utf-8');
	require_once $config['classes_dir'].DIRECTORY_SEPARATOR.'CrushRaid.php';
	spl_autoload_register(array('CrushRaid', 'loadClass'));
	
	// Might I ever consider using a upgrade script for a new version
	// we got a hook in place
	if(file_exists('upgrade.php')) {
		require 'upgrade.php';
	}
	else {
		CrushRaid::init($config);
	}
}
catch (Exception $e) {
	echo '<pre>' . $e;
}