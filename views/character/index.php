<?php $dkp_view = CrushRaid::userLoggedIn()?>
<div class="character_index">
	<table class="tablesorter">
		<thead>
		<tr>
			<th class="headerSortDown">C</th>
			<th>Name</th>
<!--			<th>Spec</th>-->
			<th>Rank</th>
			<th title="Mainrole">1</th>
			<th title="Dualspec">2</th>
			<th>Gear</th>
			<th class="headerSortUp">Dkp</th>
			<th title="Hours raided last 2 weeks">Hours Raided</th>
		</tr>
		</thead>
		<tbody>
	 	<?foreach($characters as $character):?>
	 	<tr>
	 		<td title="<?=$character->class_name?>"><img src="<?=CrushRaid::c('basepath')?>pics/classes/<?=$character->class_icon?>" alt="<?=$character->class_name?>" /></td>
	 		<td title="<?=$character->name?>"><a href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$character->id?>" style="color: #<?=$character->color?>;"><?=$character->name?></a></td>
<!--	 		<td><?=$character->spec?></td>-->
	 		<td title="<?=$character->in_guild?$character->rank_id:100?>"><?=$character->in_guild ? $character->rank : 'Not in guild'?></td>
	 		<td title="<?=$character->role?>"><?if(!empty($character->role_icon)):?><img src="<?=CrushRaid::c('basepath')?>pics/roles/<?=$character->role_icon?>" alt="<?=$character->role?>" /><?endif?></td>
	 		<td title="<?=$character->offrole?>"><?if(!empty($character->offrole_icon)):?><img src="<?=CrushRaid::c('basepath')?>pics/roles/<?=$character->offrole_icon?>" alt="<?=$character->offrole?>" /><?endif?></td>
	 		<td style="text-align: right;"><?=$character->gear_score?></td>
	 		<td title="<?=(int)$character->dkp?>" style="text-align: right;"><?if($dkp_view):?><a href="<?=CrushRaid::c('baseurl')?>/dkp/view/<?=$character->id?>"><?=(int)$character->dkp?></a><?else:?><?=(int)$character->dkp?><?endif?></td>
	 		<td style="text-align: right;"><?=round($character->raided/60/60, 1)?></td>
	 	</tr>
	 	<?endforeach?>
		</tbody>
	</table>
</div>