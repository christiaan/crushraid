<div class="character_view">
<?if(!$character->isLoaded()):?>
	Character not found
<?else:?>
	<a href="<?=CrushRaid::c('baseurl')?>/character/edit/<?=$character['id']?>">Edit</a>
	<a href="<?=CrushRaid::c('baseurl')?>/character/delete/<?=$character['id']?>">Delete</a>

<div>	
	<div><a href="<?=Armory::getCharacterUrl(CrushRaid::c('realm'), $character['name'])?>" target="_blank" style="color: #<?=$character->class['color']?>;"><?=$character['name']?></a> <?=$character->race['name']?> <?=$character->class['name']?></div>
	<div><?=$character['spec']?> <?=$character->role['name']?> (<?=$character['gear_score']?>)</div>
	<div>Current DKP: <?=$character->getCurrentDkp()?></div>
	<div>Attency last month: <?=round($character->getAttendency())?>%</div>
	
	<?if(!empty($character['parent_id'])):?>
		<div class="parent_character">This character shares DKP with: <a href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$character->parent['id']?>" style="color: #<?=$character->parent->class['color']?>;"><?=$character->parent['name']?></a></div>
	<?endif?>
	
	<?if(count($character->loot)):?>
	<fieldset class="loot">
		<legend>Looted Items</legend>
		<?foreach($character->loot as $loot):?>
		<div>
			<span class="value"><?=(0-$loot->dkp['value'])?> dkp</span>
			<?if($loot->item['wowid']):?>
				<a href="http://www.wowhead.com/?item=<?=urlencode($loot->item['wowid'])?>"><?=$loot->item['name']?></a>
			<?else:?>
				<a href="http://www.wowhead.com/?search=<?=urlencode($loot->item['name'])?>"><?=$loot->item['name']?></a>
			<?endif?>
		</div>
		<?endforeach?>
	</fieldset>
	<?endif?>
	
	<?if(count($character->dkp)):?>
	<fieldset class="dkp">
		<legend>Recent Dkp History</legend>
		<?foreach($character->dkp as $k => $dkp): if($k >= 25) break;?>
		<div><span class="value"><?=$dkp['value']?></span><span class="note"><?=htmlspecialchars($dkp['note'])?></span></div>
		<?endforeach?>
	</fieldset>
	<?endif?>
	
	<?if(count($character->attendees)):?>
	<fieldset class="attendees">
		<legend>Recent Attendency</legend>
		<?foreach($character->attendees as $k => $attendee): if($k >= 25) break;?>
		<div>
			<span class="standby_status"><?=$attendee['standby']?'standby':'in'?></span>
			<span class="raid"><a href="<?=CrushRaid::c('baseurl')?>/raid/view/<?=$attendee['raid_id']?>"><?=$attendee->raid['title']?> <?=strftime("%d %b %y", strtotime($attendee->raid['starttime']))?></a></span>
		</div>
		<?endforeach?>
	</fieldset>
	<?endif?>	
</div>
<?endif?>
</div>