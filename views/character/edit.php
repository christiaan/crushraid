<div class="character_edit">
	<form action="?" method="post" class="hotkey_submit">
	<div><?if($character->isLoaded()):?>Edit <?=$character['name']?><?else:?>Add Character<?endif?></div>
	<div>Name:</div>
	<div><input type="text" name="name" value="<?=$character['name']?>" /></div>
	<div>Active:</div>
	<div><input name="active" value="1" type="checkbox"<?if($character['active']):?> checked="checked"<?endif?> /></div>
	<div>Parent character: (dkp sharing)</div>
	<?if(0 == ActiveRecord::factory('Model_Character')->where('parent_id', $character['id'])->getCount()):?>
	<div><?=Html::select('parent_id', ActiveRecord::factory('Model_Character')->where('parent_id', 'NULL', 'IS', false)->get()->getSelect('id', 'name'), $character['parent_id'], false)?></div>
	<?else:?>
	<div>This character is a parent of other character.</div>
	<?endif?>
	<div>Leave Class or Race empty to update from the Armory.</div>
	<div>Class:</div>
	<div>
		<select name="class_id">
		<option value="0">-- pick --</option>
		<?foreach(ActiveRecord::factory('Model_CharacterClass')->get() as $item):?>
			<option value="<?=$item['id']?>" style="background-color: #<?=$item['color']?>;"<?if($item['id'] == $character['class_id']):?> selected="selected"<?endif?>><?=$item['name']?></option>
		<?endforeach?>
		</select>
	</div>
	<div>Race:</div>
	<div><?=Html::select('race_id', ActiveRecord::factory('Model_CharacterRace')->get()->getSelect('id', 'name'), $character['race_id'], false)?></div>
	<?php $roles = ActiveRecord::factory('Model_CharacterRole')->get()->getSelect('id', 'name');?>
	<div>Role:</div>
	<div><?=Html::select('role_id', $roles, $character['role_id'], false)?></div>
	<div>Dualspec:</div>
	<div><?=Html::select('offrole_id', $roles, $character['offrole_id'], false)?></div>
	<div>Level:</div>
	<div><input type="text" name="level" value="<?=$character['level']?>" /></div>
	<div>Gender:</div>
	<div><?=Html::select('gender', array(0=>'Male', 1=>'Female'), $character['gender'])?></div>
	<div>In Guild:</div>
	<div><input name="in_guild" value="1" type="checkbox"<?if($character['in_guild']):?> checked="checked"<?endif?> /></div>
	
	<div><input type="submit" value="Save" /></div>
	</form>
</div>