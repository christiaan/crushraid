<fieldset>
	<legend>Create new Raid</legend>
	<?if(!empty($errors)):?>
	<div>
		<?php foreach ($errors as $error):?>
		<div><?=ucfirst($error)?></div>
		<?endforeach?>
	</div>
	<?endif?>
	<div>
		<form action="?" method="post">
		<div>Name</div>
		<div><input type="text" name="title" /></div>
		
		<div>Headcount XML export string:</div>
		<div><textarea name="headcount" rows="7" cols="30"></textarea></div>
		
		<div><input type="submit" value="Create" /></div>
		</form>
	</div>	
</fieldset>