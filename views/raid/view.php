<div class="raid_view">
	<a href="<?=CrushRaid::c('baseurl')?>/raid/edit/<?=$raid['id']?>">Edit</a>
	<a href="<?=CrushRaid::c('baseurl')?>/raid/delete/<?=$raid['id']?>">Delete</a>

	<h1 class="title"><?=$raid['title']?> <?=$raid['starttime']?> - <?=strftime("%H:%M:%S", strtotime($raid['endtime']))?></h1>
	<?if(!$raid['finalized']):?><h2>This raid is still in progress!</h2><?endif?>
	<div class="instances">
		<img src="<?=CrushRaid::c('basepath')?>pics/instances/<?=$raid->instance['icon']?>" /> <?=$raid->instance['name']?>
	</div>
	<div>
		<div>Dkp per hour: <?=$raid['dkp_per_hour']?></div>
		<div>Standby Dkp per hour: <?=$raid['standby_per_hour']?></div>
	</div>
	<div class="notes">
		<?=$raid['notes']?>
	</div>
	<?$total = 0?>
	<fieldset class="loot">
		<legend>Loot</legend>
		<table><tbody>
		<?foreach($raid->loot as $loot):?>
		<tr>
			<td class="name">
				<a href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$loot->character['id']?>" style="color: #<?=$loot->character->class['color']?>;" class="<?=$loot->character['in_guild'] ? 'in_guild' : 'not_in_guild'?>">
					<?=$loot->character['name']?>
				</a>
			</td>
			<td class="item">
				<?if($loot->item['wowid']):?>
				<a href="http://www.wowhead.com/?item=<?=urlencode($loot->item['wowid'])?>"><?=$loot->item['name']?></a>
				<?else:?>
				<a href="http://www.wowhead.com/?search=<?=urlencode($loot->item['name'])?>"><?=$loot->item['name']?></a>
				<?endif?>
			</td>
			<td class="dkp">
				<?=(0-$loot->dkp['value'])?> dkp
				<?$total += (0-$loot->dkp['value'])?>
			</td>
			<td class="time">
				<?=strftime("%H:%M:%S", strtotime($loot['datetime']))?>
			</td>
		</tr>
		<?endforeach?>
		</tbody></table>
		<div>Total dkp spend this raid: <?=$total?></div>
	</fieldset>
	<?$total = 0?>
	<?if(count($raid->events)):?>
	<fieldset class="events">
		<legend>Events</legend>
		<?foreach($raid->events as $event):?>
		<fieldset>
			<legend><?=$event['name']?> <?=strftime("%H:%M:%S", strtotime($event['datetime']))?> <?=$event['dkp']?> dkp</legend>
			<?
			$total += $event['dkp'] * count($event->attendees);
			$sorted_keys = array();
			foreach($event->attendees as $k => $attendee) $sorted_keys[$k] = $attendee->character['name'];
			natcasesort($sorted_keys);
			foreach(array_keys($sorted_keys) as $k): $attendee = $event->attendees[$k];
			?>
			<a href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$attendee->character['id']?>" style="color: #<?=$attendee->character->class['color']?>;" class="<?=$attendee->character['in_guild'] ? 'in_guild' : 'not_in_guild'?> character">
				<?=$attendee->character['name']?>
			</a>
			<?endforeach?>
		</fieldset>
		<?endforeach?>
		Total dkp earned by events: <?=$total?>
	</fieldset>
	<?endif?>
	<?$total=0?>
	<fieldset class="attendees">
		<legend>Attendees</legend>
		<?php
			$attendees_by_class = array();
			foreach ($raid->getAttendees() as $attendee){
				if(!isset($attendees_by_class[$attendee->character['class_id']])) $attendees_by_class[$attendee->character['class_id']] = array();
				$attendees_by_class[$attendee->character['class_id']][] = $attendee;
			}
			ksort($attendees_by_class); // Classes should be alphabetically sorted in db
		foreach ($attendees_by_class as $class):
		$last_index = count($class) - 1;
		foreach ($class as $k => $attendee):
		?>
		<?if(0 == $k):?>
		<fieldset>
			<legend><?=$attendee->character->class['name']?></legend>
			<table><tbody>
		<?endif?>
		<tr>
			<td class="standby">
				<span class="status <?=$attendee['standby'] ? 'standby' : 'in'?>"></span>
			</td>
			<td class="name">
				<a href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$attendee->character['id']?>" style="color: #<?=$attendee->character->class['color']?>;" class="<?=$attendee->character['in_guild'] ? 'in_guild' : 'not_in_guild'?>">
					<?=$attendee->character['name']?>
				</a>
			</td>
			<td class="time">
				<?=strftime("%H:%M:%S", strtotime($attendee['starttime']))?> - <?=strftime("%H:%M:%S", strtotime($attendee['endtime']))?>
			</td>
			<td class="dkp">
				<?=(int) $attendee->dkp['value']?> dkp
				<?$total += $attendee->dkp['value']?>
			</td>			
		</tr>
		<?if($last_index == $k):?></tbody></table></fieldset><?endif?>
		<?endforeach;endforeach?>
		<div>Total dkp earned by attendency: <?=$total?></div>
	</fieldset>
</div>