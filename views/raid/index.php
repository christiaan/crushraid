<div id="raid_index">
<table class="tablesorter">
<thead>
	<tr>
		<th>I</th>
		<th>Title</th>
		<th class="headerSortUp">Date</th>
	</tr>
</thead>
<tbody>
<?foreach($raids as $raid):?>
	<tr>
		<td title="<?=$raid->instance['name']?>"><img src="<?=CrushRaid::c('basepath')?>pics/instances/<?=$raid->instance['icon']?>" /></td>
		<td title="<?=$raid['title']?>"><a href="<?=CrushRaid::c('baseurl')?>/raid/view/<?=$raid['id']?>"><?=$raid['title']?></a></td>
		<td title="<?=strftime("%Y%m%d", strtotime($raid['starttime']))?>"><?=strftime("%d %b %y", strtotime($raid['starttime']))?></td>
	</tr>
<?endforeach?>
</tbody>
</table>
</div>