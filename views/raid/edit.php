<div id="raid_edit">
	<?if($raid['finalized']):?><h2>This raid is already finalized!</h2><?endif?>

	<form class="hotkey_submit" action="?" method="post">
	<fieldset>
		<legend>General Details</legend>
		<div style="float: left;">
			<div>Title: <input name="raid[title]" type="text" value="<?=htmlspecialchars($raid['title'])?>" /></div>	
			<div>Notes:</div>
			<div>
				<textarea name="raid[notes]" rows="6" cols="20"><?=$raid['notes']?></textarea>
			</div>
		</div>
		<div style="float: left; margin-left: 10px;">
			<div>Start time</div>
			<div><input type="text" name="raid[starttime]" value="<?=$raid['starttime']?>" /><!--<a class="now" href="#">Now!</a>--></div>
			<div>End time</div>
			<div><input type="text" name="raid[endtime]" value="<?=$raid['endtime']?>" /><!--<a class="now" href="#">Now!</a>--></div>
		</div>
		<div style="float: left; margin-left: 10px;">
			<div>Instance:</div>
			<div class="instances">
				<?=Html::select(array('name' => 'raid[instance_id]', 'size' => 10), ActiveRecord::factory('Model_Instance')->get()->getSelect('id', 'name'), $raid['instance_id'])?>
			</div>
		</div>
		<div style="float: left; margin-left: 10px;">
			<div>Progress raid: <input type="checkbox" name="raid[progress]" value="1"<?=!empty($raid['progress'])?'checked="checked"':''?> /></div>
			<div>Dkp per hour</div>
			<div><input type="text" name="raid[dkp_per_hour]" value="<?=$raid['dkp_per_hour']?>" /></div>
			<div>Standby Dkp per hour</div>
			<div><input type="text" name="raid[standby_per_hour]" value="<?=$raid['standby_per_hour']?>" /></div>
			<?if(!$raid['finalized']):?>
			<div><input type="submit" name="finalize_raid" value="Finalize Raid" onclick="return confirm('Are you sure you want to FINALIZE this raid? DKP for the time spend in the raid will be awarded.');" /></div>
			<?else:?>
			<div><input type="submit" name="finalize_raid" value="ReFinalize Raid" onclick="return confirm('Are you sure you want to ReFINALIZE this raid? DKP for the time spend in the raid will be reeveluated.');" /></div>
			<?endif?>
		</div>
		
		<div style="clear: both;"></div>
	</fieldset>
	<div class="buttons">
		<a id="button_attendees" class="button active" href="#">Attendees</a>
		<a id="button_loot" class="button" href="#">Loot</a>
		<a id="button_events" class="button" href="#">Events</a>
	</div>	
	<fieldset id="pane_attendees" class="pane">
		<legend>Attendees</legend>
		<div>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Start</th>
						<th>End</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				<?foreach ($raid->getAttendees() as $attendee):?>
				<tr>
					<td><input type="checkbox" name="attendees[cur][<?=$attendee['id']?>][active]" checked="checked" /></td>
					<td>
						<a target="_blank" href="<?=CrushRaid::c('baseurl')?>/character/view/<?=$attendee->character['id']?>" style="color: #<?=$attendee->character->class['color']?>;" class="<?=$attendee->character['in_guild'] ? 'in_guild' : 'not_in_guild'?>">
							<?=$attendee->character['name']?>
						</a>
					</td>
					<td><input type="text" name="attendees[cur][<?=$attendee['id']?>][starttime]" value="<?=$attendee['starttime']?>" /><!--<a class="now" href="#">Now!</a>--></td>
					<td><input type="text" name="attendees[cur][<?=$attendee['id']?>][endtime]" value="<?=htmlspecialchars($attendee['endtime'])?>" /><!--<a class="now" href="#">Now!</a>--></td>
					<td>
						<input type="hidden" name="attendees[cur][<?=$attendee['id']?>][standby]" value="<?=$attendee['standby']?>" />
						<a class="status <?=$attendee['standby'] ? 'standby' : 'in'?>" href="#"></a>
					</td>
				</tr>
				<?endforeach?>
				<tr class="template" style="display: none;">
					<td><input type="checkbox" name="attendees[new][new_id][active]" checked="checked" /></td>
					<td>
						<select name="attendees[new][new_id][character_id]">
							<option value="0">-- pick --</option>
							<?foreach(ActiveRecord::factory('Model_Character')->where('level', CrushRaid::c('level'))->get() as $item):?>
							<option value="<?=$item['id']?>" style="background-color: #<?=$item->class['color']?>;"><?=$item['name']?></option>
							<?endforeach?>
						</select>
					</td>
					<td><input type="text" name="attendees[new][new_id][starttime]" /><!--<a class="now" href="#">Now!</a>--></td>
					<td><input type="text" name="attendees[new][new_id][endtime]" /><!--<a class="now" href="#">Now!</a>--></td>
					<td>
						<input type="hidden" name="attendees[new][new_id][standby]" value="0" /><a class="status in" href="#"></a>
					</td>
				</tr>
				</tbody>
			</table>
			<a href="#" class="add_row">Add</a>
		</div>
	</fieldset>
	<fieldset id="pane_loot" class="pane" style="display: none;">
		<legend>Loot</legend>
		<div>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>Looter</th>
						<th>Item</th>
						<th>Dkp cost</th>
						<th>Date Time</th>
						<th>Headcount note</th>
					</tr>
				</thead>
				<tbody>
					<?foreach ($raid->loot as $loot):?>
					<tr>
						<td><input type="checkbox" name="loot[cur][<?=$loot['id']?>][active]" checked="checked" /></td>
						<td>
							<select name="loot[cur][<?=$loot['id']?>][character_id]">
							<?foreach($raid->getCharacters() as $character):?>
								<option value="<?=$character['id']?>" style="background-color: #<?=$character->class['color']?>;"<?if($character['id'] == $loot['character_id']):?> selected="selected"<?endif?>><?=$character['name']?></option>
							<?endforeach?>
							</select>
						</td>
						<td><input type="text" name="loot[cur][<?=$loot['id']?>][itemname]" value="<?=htmlspecialchars($loot->item['name'])?>" /></td>
						<td><input type="text" name="loot[cur][<?=$loot['id']?>][dkp_cost]" value="<?=abs($loot->dkp['value'])?>" /></td>
						<td>
							<input type="text" name="loot[cur][<?=$loot['id']?>][datetime]" value="<?=htmlspecialchars($loot['datetime'])?>" /><!--<a class="now" href="#">Now!</a>-->
						</td>
						<td><?=$loot['note']?></td>
					</tr>
					<?endforeach?>
					<tr class="template" style="display: none;">
						<td><input type="checkbox" name="loot[new][new_id][active]" checked="checked" /></td>
						<td>
							<select name="loot[new][new_id][character_id]">
								<option value="0">-- pick --</option>
							<?foreach($raid->getCharacters() as $character):?>
								<option value="<?=$character['id']?>" style="background-color: #<?=$character->class['color']?>;"><?=$character['name']?> (<?=$character->getCurrentDkp()?>)</option>
							<?endforeach?>
							</select>
						</td>
						<td><input type="text" name="loot[new][new_id][itemname]" /></td>
						<td><input type="text" name="loot[new][new_id][dkp_cost]" /></td>
						<td><input type="text" name="loot[new][new_id][datetime]" /><!--<a class="now" href="#">Now!</a>--></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<a href="#" class="add_row">Add</a>
		</div>
	</fieldset>
	<fieldset id="pane_events" class="pane" style="display: none;">
		<legend>Events</legend>
		<div>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Dkp</th>
						<th>Date Time</th>
					</tr>
				</thead>
				<tbody>
					<?foreach ($raid->events as $event):?>
					<tr>
						<td><input type="checkbox" name="events[cur][<?=$event['id']?>][active]" checked="checked" /></td>
						<td><input type="text" name="events[cur][<?=$event['id']?>][name]" value="<?=htmlspecialchars($event['name'])?>" /></td>
						<td><input type="text" name="events[cur][<?=$event['id']?>][dkp]" value="<?=$event['dkp']?>" /></td>
						<td>
							<input type="text" name="events[cur][<?=$event['id']?>][datetime]" value="<?=htmlspecialchars($event['datetime'])?>" /><!--<a class="now" href="#">Now!</a>-->
						</td>
					</tr>
					<?endforeach?>
					<tr class="template" style="display: none;">
						<td><input type="checkbox" name="events[new][new_id][active]" checked="checked" /></td>
						<td><input type="text" name="events[new][new_id][name]" /></td>
						<td><input type="text" name="events[new][new_id][dkp]" /></td>
						<td><input type="text" name="events[new][new_id][datetime]" /><!--<a class="now" href="#">Now!</a>--></td>
					</tr>
				</tbody>
			</table>
			<a href="#" class="add_row">Add</a>
		</div>
	</fieldset>
	<div style="clear: both;"></div>
	<div><input type="submit" value="Save & Refresh" /></div>
	</form>
</div>