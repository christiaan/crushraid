<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=isset($title) ? $title: ''?></title>
<link rel="stylesheet" href="<?=CrushRaid::c('basepath')?>css/style.css" type="text/css"></link>

<script type="text/javascript" charset="utf-8" src="<?=CrushRaid::c('basepath')?>js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<?=CrushRaid::c('basepath')?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" charset="utf-8" src="<?=CrushRaid::c('basepath')?>js/jquery.duallist.js"></script>
<script type="text/javascript" charset="utf-8" src="<?=CrushRaid::c('basepath')?>js/jquery.hotkeys-0.7.8.js"></script>
<script type="text/javascript" src="http://www.wowhead.com/widgets/power.js"></script>
<script type="text/javascript">
jQuery(function($){
	$('select.duallist').duallist({
		template : 
			'<div class="duallist">'+
			'	<div class="duallist_selected"><select multiple="multiple"></select></div>'+
			'	<div class="duallist_buttons"><input class="duallist_select" type="button" value="Add" />&nbsp;<input class="duallist_deselect" type="button" value="Remove" /></div>'+
			'	<div class="duallist_deselected"><select multiple="multiple"></select></div>'+
			'</div>',
		autosize : true
	});

	$('.buttons a.button').click(function(){
		if(!$(this).is('.active'))
		{
			$('.pane').hide();
			$('#pane_'+$(this).attr('id').replace(/button_/gi, '')).show();
			$('.buttons a.active').removeClass('active');
			$(this).addClass('active');
		}

		return false;
	});

	$('table.tablesorter').tablesorter({ 
        // define a custom text extraction function 
        textExtraction: function(node) { 
            // extract data from markup and return it
            return $(node).attr('title') ? $(node).attr('title') : $(node).text(); 
        } 
    });

	$('body').click(function(e){
		var clicked = $(e.target);
		
		if(clicked.is('a.delete'))
		{
			clicked.parents('tr').remove();
			return false;
		}

		if(clicked.is('a.now'))
		{
			var d = new Date(),
			now = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();

			clicked.siblings('input').val(now);		
			return false;
		}

		if(clicked.is('a.status'))
		{
			if(clicked.is('.in') && clicked.siblings('input').val('1').length)
			{
				clicked.removeClass('in').addClass('standby');
				return false;
			}
			if(clicked.is('.standby') && clicked.siblings('input').val('0').length)
			{
				clicked.removeClass('standby').addClass('in');
				return false;
			}
		}

		if(clicked.is('a.add_row'))
		{
			var counter = clicked.attr('rel') ? clicked.attr('rel') : 0,
			template = clicked.siblings('table').find('tr.template'),
			temp = $('<div>'),
			new_row = template.clone().show().removeClass('template');
			
			temp.append(new_row).html(temp.html().replace(/new_id/g, counter));
			template.before(temp.html());
			clicked.attr('rel', ++counter);

			return false;
		}
	});

	if($('form.hotkey_submit').length)
	{
		function submit_form(){
			$('form.hotkey_submit').submit();
			return false;
		}
		
		$(document).bind('keydown', 'Ctrl+s', submit_form).bind('keydown', 'f2', submit_form);
	}

	/*
	$('select').change(function(e){
		var opt = $('option:selected', this);
		if(opt.css('background-color'))
		{
			$(this).css('background-color', opt.css('background-color'));
		}
	});
	*/
});
</script>
</head>
<body>
<div class="menu">
	<div class="topmenu"><a href="<?=CrushRaid::c('homepage_url')?>">Home</a>

	</div>
	<div class="topmenu"><a href="<?=CrushRaid::c('baseurl')?>/character">Characters</a>
		<div class="submenu">
			<div><a href="<?=CrushRaid::c('baseurl')?>/character?active=0">Inactive</a></div>
			<?if(CrushRaid::userLoggedIn()):?>
			<div><a href="<?=CrushRaid::c('baseurl')?>/character/edit">New Character</a></div>
			<div><a href="<?=CrushRaid::c('baseurl')?>/dkp/recalculate">Recalculate DKP</a></div>
			<?endif?>
		</div>
	</div>
	<div class="topmenu"><a href="<?=CrushRaid::c('baseurl')?>/raid">Raids</a>
		<?if(CrushRaid::userLoggedIn()):?>
		<div class="submenu">
			<div><a href="<?=CrushRaid::c('baseurl')?>/raid/new">New Raid</a></div>
		</div>
		<?endif?>
	</div>
	<?if(CrushRaid::userLoggedIn()):?>
	<div class="topmenu">Sync with
		<div class="submenu">
			<div><a href="<?=CrushRaid::c('baseurl')?>/sync/armory">Armory</a></div>
			<div><a href="<?=CrushRaid::c('baseurl')?>/sync/wowheroes">WoW Heroes</a></div>
		</div>
	</div>
	<?endif?>
	<div class="topmenu">
	<?if(CrushRaid::userLoggedIn()):?>
		<a href="<?=CrushRaid::c('baseurl')?>/account/logout">Logout</a>
	<?else:?>
		<a href="<?=CrushRaid::c('baseurl')?>/account/login">Login</a>
	<?endif?>
	</div>
	<div style="clear: both;"></div>
</div>
<?=isset($body) ? (($body instanceof View) ? $body->render() : $body) : ''?>
<p>
	<a href="http://www.wow-heroes.com" target="_blank" title="Powered by Wow Heroes"><img src="http://static.wow-heroes.com/images/logo90x42.png" width="90" height="42" border="0" alt="Wowheroes" /></a>
	<a href="http://static.wowhead.com" target="_blank" title="Powered by Wowhead"><img src="http://static.wowhead.com/images/badge_88x31.gif" width="88" height="31" border="0" alt="Wowhead" /></a> 
</p>
</body>
</html>